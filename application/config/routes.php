<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['update']           = "backEnd/BackEndController/updateApps";

$route['struk']               = "backEnd/RefUnitController/beli";
$route['serahBpkb']           = "backEnd/RefBpkbController/serahBpkb";
$route['simulasi']            = "backEnd/BackEndController/simulasi";

$route['default_controller']    = "backEnd/BackEndController";
$route['login']                   = "backEnd/BackEndController/login";
$route['logout']                  = "backEnd/BackEndController/logout";

$route['refUser']                   = "backEnd/RefUserController";
$route['refUserAdd']                = "backEnd/RefUserController/add";
$route['refUserUpdate/(:num)']      = "backEnd/RefUserController/update/$1";
$route['refUserDelete/(:num)']      = "backEnd/RefUserController/delete/$1";

$route['refUnit']                   = "backEnd/RefUnitController";
$route['refUnitAdd']                = "backEnd/RefUnitController/add";
$route['refUnitUpdate/(:num)']      = "backEnd/RefUnitController/update/$1";
$route['refUnitDelete/(:num)']      = "backEnd/RefUnitController/delete/$1";
$route['refUnitKasir/(:num)']       = "backEnd/RefUnitController/kasir/$1";
$route['refUnitPesananKasir/(:num)']       = "backEnd/RefUnitController/pesananKasir/$1";
$route['refUnitPesanan']            = "backEnd/RefUnitController/pesan";
$route['refUnitView/(:num)']        = "backEnd/RefUnitController/view/$1";
$route['refUnitExcel']              = "backEnd/RefUnitController/exportExcel";

$route['refUnitKeluar']                          = "backEnd/RefUnitKeluarController";
$route['refUnitKeluarAdd']                       = "backEnd/RefUnitKeluarController/add";
$route['refUnitKeluarCetakLaporan']              = "backEnd/RefUnitKeluarController/cetakUnit";
$route['refUnitKeluarCetak/(:num)']              = "backEnd/RefUnitKeluarController/cetakUlang/$1";
$route['refUnitKeluarView/(:num)']               = "backEnd/RefUnitKeluarController/view/$1";
$route['refUnitKeluarExcel']                     = "backEnd/RefUnitKeluarController/exportExcel";

$route['refMerk']                   = "backEnd/RefMerkController";
$route['refMerkAdd']                = "backEnd/RefMerkController/add";
$route['refMerkUpdate/(:num)']      = "backEnd/RefMerkController/update/$1";
$route['refMerkDelete/(:num)']      = "backEnd/RefMerkController/delete/$1";

$route['refVarian']                   = "backEnd/RefVarianController";
$route['refVarianAdd']                = "backEnd/RefVarianController/add";
$route['refVarianUpdate/(:num)']      = "backEnd/RefVarianController/update/$1";
$route['refVarianDelete/(:num)']      = "backEnd/RefVarianController/delete/$1";

$route['refType']                   = "backEnd/RefTypeController";
$route['refTypeAdd']                = "backEnd/RefTypeController/add";
$route['refTypeUpdate/(:num)']      = "backEnd/RefTypeController/update/$1";
$route['refTypeDelete/(:num)']      = "backEnd/RefTypeController/delete/$1";

$route['refBpkb']                   = "backEnd/RefBpkbController";
$route['refBpkbAdd']                = "backEnd/RefBpkbController/add";
$route['refBpkbUpdate/(:num)']      = "backEnd/RefBpkbController/update/$1";
$route['refBpkbDelete/(:num)']      = "backEnd/RefBpkbController/delete/$1";
$route['refBpkbLaporan/(:num)']     = "backEnd/RefBpkbController/laporan/$1";

$route['refBpkbKeluar']                   = "backEnd/RefBpkbKeluarController";
$route['refBpkbKeluarLaporan/(:num)']     = "backEnd/RefBpkbKeluarController/laporan/$1";

$route['refCustomer']                   = "backEnd/RefCustomerController";
$route['refCustomerAdd']                = "backEnd/RefCustomerController/add";
$route['refCustomerUpdate/(:num)']      = "backEnd/RefCustomerController/update/$1";
$route['refCustomerDelete/(:num)']      = "backEnd/RefCustomerController/delete/$1";
$route['refCustomerLaporan/(:num)']     = "backEnd/RefCustomerController/laporan/$1";
$route['refCustomerCetak/(:num)']       = "backEnd/RefCustomerController/cetak/$1";
$route['refCustomerBeli/(:num)']        = "backEnd/RefCustomerController/beli/$1";

// ================================= LAPORAN ====================================
$route['viewBuktiPembayaran/(:num)']   = "backEnd/RefLaporanController/viewBuktiPembayaran/$1";
$route['refPengambilAdd/(:num)']                = "backEnd/RefBpkbController/tambahPengambil/$1";

$route['404_override']      = '';
$route['translate_uri_dashes']  = FALSE;
