<?php

define('nameSite','Bahagia23Mobil');

define('assetsGLobal',base_url().'assets/global/');
define('assetsPlugin',base_url().'assets/plugins/');
define('assetsImages',base_url().'assets/images/unit/');
define('assetsBackEnd',base_url().'assets/backEnd/');

define('imagesGLobal',base_url().'assets/global/images/');


function isLogin(){
    $CI =& get_instance();
    $CI->load->library('session');
    if($CI->session->userdata('userId')) {
        // if($CI->session->userdata('TM')<time()){
        //     $CI->session->set_flashdata('error','Waktu Akses Anda Telah Habis Silakan Login Kembali');
        //     $this->doLogout();
        // }
        return true;
    }else{
        // $CI =& get_instance();
        // $pesan = "<b>Maaf</b>, Anda Belum Login, <br> Silahkan login terlebih dahulu";
        // $CI->session->set_flashdata('error',$pesan);
        return false;
    }
}