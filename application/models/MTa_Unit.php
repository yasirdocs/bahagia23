<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MTa_Unit extends MY_Model {

    var $table                    = 'ta_unit',
        $Kd_Unit                  = 'Kd_Unit',
        $Kd_Merk                  = 'Kd_Merk',
        $Kd_Varian                = 'Kd_Varian',
        $Kd_Type                  = 'Kd_Type',
        $Kd_Customer              = 'Kd_Customer',
        $Kd_Bpkb                  = 'Kd_Bpkb'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' tu')
            ->join('ref_unit ru', 'tu.Kd_Unit = tu.Kd_Unit', 'inner')
            ->join('ref_merk rm', 'rm.Kd_Merk = tu.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = tu.Kd_Varian', 'inner')
            ->join('ref_customer rc', 'rc.Kd_Customer = tu.Kd_Customer', 'inner')
            ->join('ref_bpkb rb', 'rb.Kd_Bpkb = tu.Kd_Bpkb', 'inner')
        ;
    }

}