<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MRef_Bpkb_Keluar extends MY_Model {

    var $table                      = 'ref_bpkb_keluar'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' rbk')
        ;
    }
}

/* End of file MRef_Bpkb_Keluar.php */
/* Location: ./application/models/MRef_Bpkb_Keluar.php */