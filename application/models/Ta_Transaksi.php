<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class Ta_Transaksi extends MY_Model {

    var $table                      = 'ta_transaksi',
        $Kd_Unit                    = 'Kd_Unit',
        $No_Plat                    = 'No_Plat',
        $Nilai_Terjual              = 'Nilai_Terjual',
        $Fee_Broker_Jual            = 'Fee_Broker_Jual',
        $Insentif                   = 'Insentif',
        $Profit                     = 'Profit',
        $Discount                   = 'Discount',
        $Refund                     = 'Refund',
        $Total_Profit               = 'Total_Profit',
        $Nm_Pembeli                 = 'Nm_Pembeli',
        $No_Ktp                     = 'No_Ktp',
        $No_Hp                      = 'No_Hp',
        $Alamat_Pembeli             = 'Alamat_Pembeli',
        $Tgl_Pembelian              = 'Tgl_Pembelian',
        $Kd_Pesanan                 = 'Kd_Pesanan'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' tt')
            ->join('ref_unit ru', 'ru.Kd_Transaksi = tt.Kd_Transaksi', 'inner')
            ->join('ref_merk rm', 'rm.Kd_Merk = ru.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = ru.Kd_Varian', 'inner')
            ->join('ref_type rt', 'rt.Kd_Type = ru.Kd_Type', 'inner')
        ;
    }

}