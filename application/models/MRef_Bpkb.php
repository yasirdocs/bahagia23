<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MRef_Bpkb extends MY_Model {

    var $table                      = 'ref_bpkb',
        $Kd_Status_Bpkb             = 'Kd_Status_Bpkb',
        $Kd_Unit                    = 'Kd_Unit',
        $Nm_Bpkb                    = 'Nm_Bpkb',
        $No_Bpkb                    = 'No_Bpkb',
        $No_Rangka                  = 'No_Rangka',
        $No_Mesin                   = 'No_Mesin',
        $Alamat_Bpkb                = 'Alamat_Bpkb',
        $Keterangan_Bpkb            = 'Keterangan_Bpkb',
        $Tgl_Masuk_Bpkb             = 'Tgl_Masuk_Bpkb',
        $Tgl_Penyerahan_Bpkb        = 'Tgl_Penyerahan_Bpkb',
        $Nm_Pengambil               = 'Nm_Pengambil',
        $Alamat_Pengambil           = 'Alamat_Pengambil',
        $No_Ktp_Pengambil           = 'No_Ktp_Pengambil'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' rb')
            ->join('ref_unit ru', 'ru.Kd_Unit = rb.Kd_Unit', 'inner')
            ->join('ref_status_bpkb rsb', 'rsb.Kd_Status_Bpkb = rb.Kd_Status_Bpkb', 'inner')
        ;
    }
}

/* End of file MRef_Bpkb.php */
/* Location: ./application/models/MRef_Bpkb.php */