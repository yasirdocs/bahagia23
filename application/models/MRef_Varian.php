<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MRef_Varian extends MY_Model {

    var $table          = 'ref_varian',
        $Kd_Merk        = 'Kd_Merk',
        $Nm_Varian      = 'Nm_Varian'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' rv')
            ->join('ref_merk rm', 'rm.Kd_Merk = rv.Kd_Merk', 'inner')
        ;
    }
}

/* End of file MRef_Varian.php */
/* Location: ./application/models/MRef_Varian.php */