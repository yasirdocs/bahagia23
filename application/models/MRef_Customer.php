<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MRef_Customer extends MY_Model {

    var $table                  = 'ref_customer',
        $Kd_Unit                = 'Kd_Unit',
        $Nm_Customer            = 'Nm_Customer',
        $Alamat_Customer        = 'Alamat_Customer',
        $No_Ktp                 = 'No_Ktp',
        $No_Hp                  = 'No_Hp',
        $Kd_Bpkb                = 'Kd_Bpkb'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' rc')
            ->join('ref_unit ru', 'ru.Kd_Unit = rc.Kd_Unit', 'inner')
            ->join('ref_merk rm', 'rm.Kd_Merk = ru.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = ru.Kd_Varian', 'inner')
            ->join('ref_type rt', 'rt.Kd_Type = ru.Kd_Type', 'inner')
            ->join('ref_bpkb rb', 'rb.Kd_Bpkb = rc.Kd_Bpkb', 'inner')
        ;
    }
    
}

/* End of file MRef_Customer.php */
/* Location: ./application/models/MRef_Customer.php */