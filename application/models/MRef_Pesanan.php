<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class MRef_Pesanan extends MY_Model {

    var $table                  = 'ref_pesanan',
        $Dp_Pesanan             = 'Dp_Pesanan',
        $Total_Bayar            = 'Total_Bayar',
        $Tgl_Pelunasan          = 'Tgl_Pelunasan'

    ;

    function getQuery($select='*'){
        return $this->db->select($select)
            ->from($this->table.' mrp');
            // ->get()
            // ->result();
    }

}