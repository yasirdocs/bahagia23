<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MRef_Type extends MY_Model {

    var $table          = 'ref_type',
        $Kd_Merk        = 'Kd_Merk',
        $Kd_Varian      = 'Kd_Varian',
        $Nm_Type        = 'Nm_Type'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' rt')
            ->join('ref_merk rm', 'rm.Kd_Merk = rt.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = rt.Kd_Varian', 'inner')
        ;
    }
}

/* End of file MRef_Type.php */
/* Location: ./application/models/MRef_Type.php */