<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MRef_Unit extends MY_Model {

    var $table                    = 'ref_unit',
        $Tahun                    = 'Tahun',
        $Nm_Unit                  = 'Nm_Unit',
        $No_Plat                  = 'No_Plat',
        $Kd_Status                = 'Kd_Status',
        $Kd_Merk                  = 'Kd_Merk',
        $Kd_Varian                = 'Kd_Varian',
        $Kd_Type                  = 'Kd_Type',
        $Tgl_Masuk                = 'Tgl_Masuk',
        $Tgl_Keluar               = 'Tgl_Keluar',
        $Harga_Beli               = 'Harga_Beli',
        $Biaya_Perawatan          = 'Biaya_Perawatan',
        $Biaya_Broker             = 'Biaya_Broker',
        $Total_Modal              = 'Total_Modal',
        $Harga_Pasar              = 'Harga_Pasar',
        $Foto_Unit                = 'Foto_Unit',
        $Kd_Transaksi             = 'Kd_Transaksi',
        $Kd_Pesanan               = 'Kd_Pesanan',
        $No_Rangka                = 'No_Rangka',
        $No_Mesin                 = 'No_Mesin'
    ;

    function getQuery(){
        return $this->db->select('*')
            ->from($this->table.' ru')
            ->join('ref_merk rm', 'rm.Kd_Merk = ru.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = ru.Kd_Varian', 'inner')
            ->join('ref_type rt', 'rt.Kd_Type = ru.Kd_Type', 'inner')
            ->join('ref_status rs', 'rs.Kd_Status = ru.Kd_Status', 'inner')
            ->join('ta_transaksi tt', 'tt.Kd_Transaksi = ru.Kd_Transaksi', 'left')
            ->join('ref_pesanan rp', 'rp.Kd_Pesanan = ru.Kd_Pesanan', 'left')
        ;
    }

    function getQueryUnit($join){
        return $this->db->select('*')
            ->from($this->table.' ru')
            ->join('ref_merk rm', 'rm.Kd_Merk = ru.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = ru.Kd_Varian', 'inner')
            ->join('ref_type rt', 'rt.Kd_Type = ru.Kd_Type', 'inner')
            ->join('ref_status rs', 'rs.Kd_Status = ru.Kd_Status', 'inner')
            ->join('ta_transaksi tt', 'tt.Kd_Transaksi = ru.Kd_Transaksi', $join)
        ;
    }

    function getQueryInner(){
        return $this->db->select('*')
            ->from($this->table.' ru')
            ->join('ref_merk rm', 'rm.Kd_Merk = ru.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = ru.Kd_Varian', 'inner')
            ->join('ref_type rt', 'rt.Kd_Type = ru.Kd_Type', 'inner')
            ->join('ref_status rs', 'rs.Kd_Status = ru.Kd_Status', 'inner')
            ->join('ta_transaksi tt', 'tt.Kd_Transaksi = ru.Kd_Transaksi', 'left')
            ->where('tt.Kd_Transaksi is null', NULL)
            ->get()->result();
        ;
    }

    function getQueryRight(){
        return $this->db->select('*')
            ->from($this->table.' ru')
            ->join('ref_merk rm', 'rm.Kd_Merk = ru.Kd_Merk', 'inner')
            ->join('ref_varian rv', 'rv.Kd_Varian = ru.Kd_Varian', 'inner')
            ->join('ref_type rt', 'rt.Kd_Type = ru.Kd_Type', 'inner')
            ->join('ref_status rs', 'rs.Kd_Status = ru.Kd_Status', 'inner')
            ->join('ta_transaksi tt', 'tt.Kd_Transaksi = ru.Kd_Transaksi', 'right')
            ->get()->result();
        ;
    }

}