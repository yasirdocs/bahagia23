<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class Map_user extends MY_Model {

    var $table                  = 'ap_user',
        $username               = 'username',
        $password               = 'password',
        $Kd_Level_Akses         = 'Kd_Level_Akses'
    ;

    function getQuery($select='*'){
        return $this->db->select($select)
            ->from($this->table.' ap');
            // ->get()
            // ->result();
    }

    function getAkses($select='*'){
        return $this->getQuery()
            ->join('ap_user_akses aua','aua.Kd_Level_Akses = ap.Kd_Level_Akses','inner')
            ->get()->result();
    }
    
    function cekLogin($username,$password,$level_akses){
        $query=$this->getQuery()
            ->where('username',$username)
            ->where('password',$password)
            ->where('Kd_Level_Akses',$level_akses)
            ->get();

        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
    }

}