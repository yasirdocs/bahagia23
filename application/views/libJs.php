<script src="<?= assetsPlugin ?>jQuery/jquery-3.2.1.min.js"></script>
<script src="<?= assetsPlugin ?>jQueryUI/jquery-ui.min.js"></script>
<script src="<?= assetsPlugin ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?= assetsPlugin ?>moment/moment.min.js"></script>
<script src="<?= assetsPlugin ?>moment/moment-with-locales.js"></script>
<script src="<?= assetsPlugin ?>bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?= assetsPlugin ?>AdminLTE/js/app.min.js"></script>
<script src="<?= assetsPlugin ?>datatables/jquery.dataTables.min.js"></script>
<script src="<?= assetsPlugin ?>datatables/dataTables.bootstrap.min.js"></script>
<script src="<?= assetsPlugin ?>sweetalert/sweetalert2.min.js"></script>
<!-- <script src="https://unpkg.com/sweetalert@2.0.8/dist/sweetalert.min.js"></script> -->
<script src="<?= assetsPlugin ?>select2/select2.min.js"></script>
<script src="<?= assetsPlugin ?>jQuery/jquery-number.min.js"></script>
<script src="<?= assetsPlugin ?>chart.min.js"></script>
<script src="<?= assetsPlugin ?>main.js"></script>

<script>
  $(document).ready(function(){
      $('.uang').number( true, 0, ',', '.' );
      select();
      $(".dataTable").getDataTable();
      // $('.form-select').select2();
  });

  function select(){
    var select = $('select').length;
    if(select == null || select == "undefined" || select == 0){return false;}
    for(i=1; i<=select; i++) {
        var label = $('#label_'+i);
        var id = label.html();
        if(id == "undefined" || id == null){var id="";}
        $('#select_'+i).select2({
            placeholder: 'Silahkan Pilih '+id,
            allowClear: true
        });
    }
  }

  $.fn.getDataTable = function( options ) {

        var opt = $.extend({
            linked   : '',
            req  : {}
        }, options );

        var me=this;
        var tes;
        if(opt.linked != ''){
            var th=(me.find('thead th').length)/2;
            var link=base_url+'backEnd/'+opt.linked;
            me.DataTable({
                "searching": false,
                "order": [],
                "responsive": true,
                "bSort": false,
                "aaSorting": [],
                "columnDefs": [{
                    "defaultContent": "-",
                    "targets": "_all"
                }],
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: link,
                    type: "post",
                    data:function ( d ) {
                        var dt_params = me.data('dt_params');
                        if(dt_params){ $.extend(d, dt_params); }
                    },
                    error: function(){
                        me.append('<tbody class="employee-grid-error"><tr><th colspan="'+th+'">Data tidak ditemukan</th></tr></tbody>');
                    }
                }
               //  "processing": true,
               // "serverSide": true,
               // "ajaxSource": link,
               //  "fnServerData": function ( sSource, aoData, fnCallback ) {
               //      $.ajax( {
               //          "dataType": 'json',
               //          "type": "POST",
               //          "url": sSource,
               //          "data": aoData,
               //          "success": fnCallback
               //      });
               //  }
               // dataSrc : function (json) {
                 // if(json.rekening){
                 //     $('#rekening tbody').html("");
                 //     $.each(json.rekening,function(key,value){
                 //         $('#rekening tbody').append("<tr><td>"+value.kode+"</td><td>"+value.nama+"</td></tr>");
                 //     });
                 // }
             //     return json.data;
            // },
            //     "scrollY":        400,
            //     "scrollCollapse": true,
            //     "paging":         false

            });
        }else{
            me.DataTable({
                "searching": true,
                "bLengthChange": false,
                "responsive": true,
                "order": [],
                "aaSorting": [],
                "bSort": false,
                "columnDefs": [{
                    "defaultContent": "-",
                    "targets": "_all"
                }]
            });
        }
    };
</script>
<!-- created by : yasirarif, website: yasirdocs.github.io  -->