<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $pageTitle ?></title>
    <link rel="stylesheet" type="text/css" href="<?= assetsPlugin ?>css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<?= assetsPlugin ?>css/templateLogin.css"/>
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="title">
                    <h2><?= nameSite ?> Aplikasi</h2>
                </div>
                <div class="account-wall">
                    <div id="my-tab-content" class="tab-content">
                        <div class="tab-pane active" id="login">
                            <!-- <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt=""> -->
                            <form class="form-signin" action="<?= current_url() ?>" method="post">
                                <p><?php flashMessage(); ?></p>
                                <select class="form-control" id="level" required="" name="Kd_Level_Akses">
                                    <?php foreach ($namelevel as $value) { ?>
                                        <option value='<?= $value->Kd_Level_Akses ?>'> <?= $value->level_akses ?> </option>
                                    <?php } ?>
                                </select>
                                    <input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
                                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                                    <button type="submit" class="btn btn-default" style="float:right;">Masuk</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <p style="text-align:center;"><b><?= nameSite ?> © 2017</b></p>
            </div>
        </div>
    </div>
  </body>
<?php $this->load->view('libJs'); ?>
<script type="text/javascript">
    // Find the right method, call on correct element
    function launchFullScreen(element) {
      if(element.requestFullScreen) {
        element.requestFullScreen();
      } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
      } else if(element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
      }
    }

    // Launch fullscreen for browsers that support it!
    launchFullScreen(document.documentElement); // the whole page
    launchFullScreen(document.getElementById("videoElement"));
</script>
</html>
