<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <div class="col-md-12">
        <?php flashMessage(); ?>
    </div>
    <h2><?= $pageTitle ?></h2>
    <br>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Harga Jual</label>
                    <input class="form-control uang" id="Harga_Jual" type="text" name="Harga_Jual" placeholder="Harga Jual" maxlength="40" value="" required>
                </div>
                <div class="form-group">
                    <label>Rekondisi</label>
                    <input class="form-control uang" id="Rekondisi" type="text" name="Rekondisi" value="" readonly="true">
                </div>
                <div class="form-group">
                    <label>TAC</label>
                    <input class="form-control uang" type="text" id="TAC" name="TAC" placeholder="TAC" maxlength="40" value="" required>
                </div>
                <div class="form-group">
                    <label>Bunga Asuransi (*%)</label>
                    <input class="form-control" type="text" name="Bunga_Asuransi" id="Bunga_Asuransi" placeholder="Bunga Asuransi" maxlength="2" value="" required>
                </div>
                <div class="form-group">
                    <label>Administrasi</label>
                    <input class="form-control uang" type="text" name="Biaya_Admin" id="Biaya_Admin" placeholder="Administrasi" maxlength="40" value="" required>
                </div>
                <div class="form-group">
                    <label>Asuransi</label>
                    <input type="text" class="form-control uang" value="" id="Asuransi" name="Asuransi" readonly="true">
                </div>
                <div class="form-group">
                    <label>Rate (*%)</label>
                    <input class="form-control" type="text" name="Rate" id="Rate" placeholder="Rate" maxlength="2" value="" required>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body"> 
                <div class="form-group">
                    <div class="form-group">
                        <label>Dp Gross</label>
                        <input class="form-control uang" type="text" name="Dp_Gross" id="Dp_Gross" placeholder="Dp Gross" value="">
                    </div>
                    <div class="form-group">
                        <label>OTR</label>
                        <input class="form-control uang" type="text" name="OTR" id="OTR" placeholder="OTR" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label>DP Net</label>
                        <input class="form-control uang" type="text" name="DP_Net" id="DP_Net" placeholder="DP Net" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label>Pokok Hutang</label>
                        <input class="form-control uang" type="text" name="Pokok_Hutang" id="Pokok_Hutang" placeholder="Pokok Hutang" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label>Total Piutang</label>
                        <input class="form-control uang" type="text" name="Total_Piutang" id="Total_Piutang" placeholder="Total Piutang" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tenor</label>
                        <input class="form-control uang" type="text" name="Tenor" id="Tenor" placeholder="Tenor" value="" maxlength="2">
                    </div>
                    <div class="form-group">
                        <label>Angsuran Perbulan</label>
                        <input class="form-control uang" type="text" name="Angsuran_Perbulan" id="Angsuran_Perbulan" placeholder="Angsuran Perbulan" value="" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('libJs'); ?>

<script>

    //fungsi untuk mengambil nilai OTR (Harga)
    $(document).on('keyup', '#Harga_Jual, #OTR, #TAC', function(){
        var val = 10; //persen
        var total = 0;
        $('#Harga_Jual').each(function () {
            if ($(this).val() != 0 && $(this).val() != null) {
                val /= 100;
                val *= $(this).val();
            }
        })
        $("input#Rekondisi").val((val <= 10) ? 0 : val);
        var TAC = $('#TAC').val();

        total = parseInt(val) + parseInt($('#Harga_Jual').val());

        if(TAC != 0){
            totalByTAC = parseInt(total) + parseInt(TAC);
            $("input#OTR").val(totalByTAC);
        }else{
            $("input#OTR").val(total);
        }
    });

    $(document).on('keyup', '#DP_Net, #Dp_Gross, #Biaya_Admin, #Asuransi', function(){
        var valGross = $('#Dp_Gross').val();
        var BiayaAdmin = $('#Biaya_Admin').val();
        var Asuransi =  $('#Asuransi').val();
        var OTR = $("#OTR").val();
        var Rate = $("#Rate").val() / 100;
        var Admin = parseInt(BiayaAdmin) + parseInt(Asuransi);
        var total = parseInt(valGross) - parseInt(Admin);

        if(total != 0 && Admin != 0){
            var total_Hutang = parseInt(OTR) - parseInt(total);
            $("input#Pokok_Hutang").val(total_Hutang);
            $("input#DP_Net").val(total);
        }else{
            $("input#DP_Net").val(total);
        }
        
    });

    $(document).on('keyup', '#Pokok_Hutang, #Rate, #Tenor', function(){
        var DP_Net = $('#DP_Net').val();
        var OTR = $('#OTR').val();
        var Rate = $('#Rate').val();
        var Tenor = $('#Tenor').val();

        var total = parseInt(OTR) - parseInt(DP_Net);
        var total_hutang = $("input#Pokok_Hutang").val(total);
        
        if(total_hutang != 0 && Rate != 0){
            var total_piutang = parseInt(total) * parseInt(Rate);
            var hasil_piutang = total_piutang/100;
            $("input#Total_Piutang").val(hasil_piutang);
            if(Tenor != 0){
                var totalHutangPiutang = parseInt(total) + parseInt(hasil_piutang);
                var totalAngsuran = totalHutangPiutang / Tenor;
                $("input#Angsuran_Perbulan").val(totalAngsuran);
            }
        }
    });

    //fungsi untuk mengambil nilai asuransi
    $(document).on('keyup', '#Bunga_Asuransi, #OTR', function(){
        var valBunga = $('#Bunga_Asuransi').val();
        var valBungaAsuransi = valBunga/100; //persen
        var valHarga = parseInt($('#OTR').val()); 
        
        var total = valHarga * valBungaAsuransi;
        $("input#Asuransi").val(total);
    });

</script>

<?php $this->load->view('footer'); ?>