<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<style type="text/css">
    .inner{
        margin-bottom:15px;
    }
</style>
<section class="content">
    <div class="col-md-12">
        <?php flashMessage(); ?>
    </div>
    <h2>Dashboard<!-- <span style="font-size:12px;"> - ShortenLink</span> --></h2>
    <br><h4 style="color:red">Selamat datang <b><?= $this->session->username ?>.</b></h4>
    <br>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
            <h3>Simulasi</h3>
            <p>Anggsuran&nbsp;</p>
        </div>
        <div class="icon"><i class="fa fa-calculator"></i></div>
        <a href="<?= base_url('simulasi') ?>" class="small-box-footer">
            Detail
            <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
            <h3><?= isset($refUnit) ? $refUnit : 0 ?></h3>
            <p>Stok Unit</p>
        </div>
        <div class="icon"><i class="fa fa-car"></i></div>
        <a href="<?= base_url('refUnit') ?>" class="small-box-footer">
            Detail
            <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
            <h3><?= isset($refUnitKeluar) ? $refUnitKeluar : 0 ?></h3>
            <p>Unit Terjual</p>
        </div>
        <div class="icon"><i class="fa fa-shopping-cart"></i></div>
        <a href="<?= base_url('refUnitKeluar') ?>" class="small-box-footer">
            Detail
            <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-teal">
        <div class="inner">
            <h3><?= isset($refBpkb) ? $refBpkb : 0 ?></h3>
            <p>Total Bpkb</p>
        </div>
        <div class="icon"><i class="fa fa-credit-card"></i></div>
        <a href="<?= base_url('refBpkb') ?>" class="small-box-footer">
            Detail
            <i class="fa fa-arrow-circle-right"></i>
        </a>
      </div>
    </div>
  <!-- <div width="1" height="1">
    <canvas id="myChart"></canvas>
</div> -->
</section>

<?php $this->load->view('libJs'); ?>
<script type="text/javascript">
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>
<?php $this->load->view('footer'); ?>