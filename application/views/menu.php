<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header"><p style="text-align:center;">Menu</p></li>
            <li class="active"><a href="<?= base_url('/') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a></li>
            <?php if($this->session->userdata('level_akses') == 1){ ?>
                <li><a href="<?= base_url('refUser') ?>"><i class="fa fa-user"></i> <span>Manajemen User</span> </a></li>
            <?php } ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-link"></i> <span>Parameter</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?= base_url('refMerk') ?>"><i class="fa fa-circle-o text-aqua"></i>Merk</a></li>
                        <li><a href="<?= base_url('refVarian') ?>"><i class="fa fa-circle-o text-aqua"></i>Varian</a></li>
                        <li><a href="<?= base_url('refType') ?>"><i class="fa fa-circle-o text-aqua"></i>Type</a></li>
                    </ul>
                </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Unit</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= base_url('refUnit') ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Stok Unit</span> </a></li>
                    <li><a href="<?= base_url('refUnitPesanan') ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Stok Pesanan</span> </a></li>
                    <li><a href="<?= base_url('refUnitKeluar') ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Stok Unit Keluar</span> </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>BPKB</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= base_url('refBpkb') ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Input BPKB</span> </a></li>
                    <li><a href="<?= base_url('refBpkbKeluar') ?>"><i class="fa fa-circle-o text-aqua"></i> <span>BPKB Keluar</span> </a></li>
                </ul>
            </li>
            <!-- <li><a href="<?= base_url('refCustomerAdd') ?>"><i class="fa fa-link"></i> <span>Kasir</span> </a></li> -->
            <!-- <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Laporan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= base_url('viewBuktiPembayaran/1') ?>"><i class="fa fa-circle-o text-aqua"></i>Bukti Pembayaran</a></li>
                </ul>
            </li> -->
            <li><a href="<?= base_url('simulasi') ?>"><i class="fa fa-calculator"></i> <span>Simulasi Angsuran</span> </a></li>
            <li><a href="<?= base_url('update') ?>"><i class="fa fa-gear"></i> <span>Update Aplikasi</span> </a></li>
            <li><a href="<?= base_url('logout') ?>"><i class="fa fa-sign-out"></i> <span>Keluar</span> </a></li>
        </ul>
<!-- <div class="sidebar-footer hidden-small">
<a href="#"> <i class="fa fa-home"></i> detect</a>
<a href="#"> <i class="fa fa-home"></i> detect</a>
<a href="#"> <i class="fa fa-home"></i> detect</a>
<a href="#"> <i class="fa fa-home"></i> detect</a>
</div> -->
    </section>
</aside>


<div class="content-wrapper">