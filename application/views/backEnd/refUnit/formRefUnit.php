<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <h4><?= $pageTitle ?></h4>
            <br>
            <div class="box box-primary">
                <form action="<?=  current_url() ?>" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Tahun</label>
                            <input type="text" name="Tahun" placeholder="Tahun" class="form-control" value="<?= isset($thisData) ? $thisData->Tahun : null ?>" maxlength="4" required>  
                        </div>
                        <div class="form-group">
                            <label id="label_1">Nama Merk Unit</label>
                            <select id="select_1" name="Kd_Merk" class="form-control" required>
                                <option></option>
                                <?php foreach ($dataMerk as $key => $value) { ?>
                                <option <?php if(isset($thisData)){ if($thisData->Kd_Merk==$value->Kd_Merk){ echo 'selected'; } }  ?> value="<?= $value->Kd_Merk ?>"><?= $value->Nm_Merk ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label id="label_2">Nama Varian Unit</label>
                            <select id="select_2" name="Kd_Varian" class="form-control" required>
                                <option></option>
                                <?php foreach ($dataVarian as $key => $value) { ?>
                                <option <?php if(isset($thisData)){ if($thisData->Kd_Varian==$value->Kd_Varian){ echo 'selected'; } }  ?> value="<?= $value->Kd_Varian ?>"><?= $value->Nm_Varian ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label id="label_3">Nama Type Unit</label>
                            <select id="select_3" name="Kd_Type" class="form-control" required>
                                <option></option>
                                <?php foreach ($dataType as $key => $value) { ?>
                                <option <?php if(isset($thisData)){ if($thisData->Kd_Type==$value->Kd_Type){ echo 'selected'; } }  ?> value="<?= $value->Kd_Type ?>"><?= $value->Nm_Type ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Unit</label>
                            <input type="text" name="Nm_Unit" placeholder="Nama Unit" class="form-control" value="<?= isset($thisData) ? $thisData->Nm_Unit : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Nomor Plat</label>
                            <input type="text" name="No_Plat" placeholder="Nomor Plat" class="form-control" value="<?= isset($thisData) ? $thisData->No_Plat : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Nomor Rangka</label>
                            <input type="text" name="No_Rangka" placeholder="Nomor Rangka" class="form-control" value="<?= isset($thisData) ? $thisData->No_Rangka : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Nomor Mesin</label>
                            <input type="text" name="No_Mesin" placeholder="Nomor Mesin" class="form-control" value="<?= isset($thisData) ? $thisData->No_Mesin : null ?>" required>  
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Harga Pasar</label>
                            <input type="text" name="Harga_Pasar" placeholder="Harga Pasar" class="form-control uang" value="<?= isset($thisData) ? $thisData->Harga_Pasar : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Harga Beli</label>
                            <input type="text" name="Harga_Beli" placeholder="Harga Beli" class="form-control uang" value="<?= isset($thisData) ? $thisData->Harga_Beli : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Biaya Perawatan</label>
                            <input type="text" name="Biaya_Perawatan" placeholder="Biaya Perawatan" class="form-control uang" value="<?= isset($thisData) ? $thisData->Biaya_Perawatan : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Biaya Broker</label>
                            <input type="text" name="Biaya_Broker" placeholder="Biaya Broker" class="form-control uang" value="<?= isset($thisData) ? $thisData->Biaya_Broker : null ?>" required>  
                        </div>
                        <div class="form-group">
                            <label>Foto Unit</label>
                            <input type="file" name="Foto_Unit" value="<?= isset($thisData) ? $thisData->Foto_Unit : null ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</section>

<?php $this->load->view('libJs'); ?>
<script type="text/javascript">
    $('button').click(function(){
        if($("input[name='Foto_Unit']").val()==0){
            swal('Informasi', 'Maaf, Foto masih kosong', 'warning');
            return false;
        }
    });
</script>
<?php $this->load->view('footer'); ?>