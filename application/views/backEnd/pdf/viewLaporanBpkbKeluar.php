<html>
<head>
    <title><?= $pageTitle ?></title>
</head>
<body>
    <h2 style="text-align:center;font-weight:bold;">Laporan Bpkb Keluar</h2>
    <br>
    <table border="1" cellspacing="0" cellpadding="6"> 
        <tr>
            <th>No</th>
            <th>Nama Bpkb</th>
            <th>No Bpkb</th>
            <th>Alamat Bpkb</th>
            <th>Nomor Rangka</th>
            <th>No Mesin</th>
            <th>Nama Pengambil</th>
            <th>No Ktp</th>
            <th>Alamat</th>
            <th>Tanggal Pengambilan</th>
            <!-- <th>No Polisi</th> -->
        </tr>
        <tr>
            <td>1</td>
            <td><?= $dataUnit->Nm_Bpkb ?></td>
            <td><?= $dataUnit->No_Bpkb ?></td>
            <td><?= $dataUnit->Alamat_Bpkb ?></td>
            <td><?= $dataUnit->No_Rangka ?></td>
            <td><?= $dataUnit->No_Mesin ?></td>
            <td><?= $dataUnit->Nm_Pengambil ?></td>
            <td><?= $dataUnit->No_Ktp_Pengambil ?></td>
            <td><?= $dataUnit->Alamat_Pengambil ?></td>
            <td><?= $dataUnit->Tgl_Penyerahan_Bpkb ?></td>
        </tr>
    </table>
</body>
</html>