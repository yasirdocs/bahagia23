<html>
<head>
    <title><?= $pageTitle ?></title>
</head>
<body style="font-size:10px;">
    <h2 style="text-align:center;font-weight:bold;">Kwitansi Pembayaran</h2>
    <br>
    <table width="100%" border="0">
        <tr>
            <td width="60%">
                <table>
                    <tr>
                        <td>Kepada Yth :</td>
                    </tr>
                    <tr>
                        <td><?= $thisData->Nm_Pembeli ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table>
                    <tr>
                        <td>Tanggal Pembelian</td>
                        <td>: <?= $thisData->Tgl_Pembelian ?></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            ( Apabila Penerima tidak sesuai dengan permintaan, harus diberitahukan terlebih dahulu )
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <p>Dengan Hormat,</p>
    <p>Dengan ini kami beritahukan, bahwa kami telah menyetujui permohonan pembiayaan dari konsumen<br>
    untuk pembelian 1 (satu) unit kendaraan dengan spesifikasi berikut :</p>

    <table style="margin-left:20px;">
        <tr>
            <td>Nama Konsumen</td>
            <td>:</td>
            <td><?= $thisData->Nm_Pembeli ?></td>
        </tr>
        <tr>
            <td>No Ktp</td>
            <td>:</td>
            <td><?= $thisData->No_Ktp ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?= $thisData->Alamat_Pembeli ?></td>
        </tr>
    </table>
    <br>
    <!-- <table border="0" width="100%">
        <tr>
            <td>
                <table border="1" width="100%" cellspacing="0" cellpadding="3">
                    <tr>
                        <td colspan="3">Produk Pembiayaan</td>
                    </tr>
                    <tr>
                        <td>Nama Unit</td>
                        <td style="padding:0px;margin:0px;text-align:center;">:</td>
                        <td><?= $thisData->Nm_Unit ?></td>
                    </tr>
                    <tr>
                        <td>Merk</td>
                        <td style="padding:0px;margin:0px;text-align:center;">:</td>
                        <td><?= $thisData->Nm_Merk ?></td>
                    </tr>
                    <tr>
                        <td>Varian</td>
                        <td style="padding:0px;margin:0px;text-align:center;">:</td>
                        <td><?= $thisData->Nm_Varian ?></td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td style="padding:0px;margin:0px;text-align:center;">:</td>
                        <td><?= $thisData->Nm_Type ?></td>
                    </tr>
                    <tr>
                        <td>Nomor Polisi</td>
                        <td style="padding:0px;margin:0px;text-align:center;">:</td>
                        <td><?= $thisData->No_Plat ?></td>
                    </tr>
                    <tr>
                        <td>Tahun</td>
                        <td style="padding:0px;margin:0px;text-align:center;">:</td>
                        <td><?= $thisData->Tahun ?></td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align:top;">
                <table border="1" width="100%" cellspacing="0" cellpadding="3">
                    <tr>
                        <td colspan="3">Skema Pelunasan</td>
                    </tr>
                    <tr>
                        <td >Harga Jual Per Unit</td>
                        <td style="margin:0px;text-align:center;">:</td>
                        <td>Rp. <?= number_format($thisData->Nilai_Terjual,0,',','.') ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table> -->

    <table width="100%" cellspacing="0" cellpadding="4" border="1">
        <tr>
            <td colspan="2" style="text-align:center;font-weight:bold">Produk Pembiayaan</td>
        </tr>
        <tr>
            <td>Nama Unit</td>
            <td><?= $thisData->Nm_Unit ?></td>
        </tr>
        <tr>
            <td>Merk</td>
            <td><?= $thisData->Nm_Merk ?></td>
        </tr>
        <tr>
            <td>Varian</td>
            <td><?= $thisData->Nm_Varian ?></td>
        </tr>
        <tr>
            <td>Type</td>
            <td><?= $thisData->Nm_Type ?></td>
        </tr>
        <tr>
            <td>Nomor Polisi</td>
            <td><?= $thisData->No_Plat ?></td>
        </tr>
        <tr>
            <td>Tahun</td>
            <td><?= $thisData->Tahun ?></td>
        </tr>
    </table>
    <br>
    <table border="1" width="100%" cellspacing="0" cellpadding="4">
        <tr>
            <td colspan="2" style="text-align:center;font-weight:bold">Skema Pelunasan</td>
        </tr>
        <tr>
            <td >Harga Jual Per Unit</td>
            <td>Rp. <?= number_format($thisData->Nilai_Terjual,0,',','.') ?></td>
        </tr>
    </table>
    <p>Pencairan Dana atas pembelian kendaaran oleh konsumen dapat kami lakukan, apabila kendaaran <br> tersebut diatas telah diserahkan kepada konsumen dengan baik dan syarat-syarat ketentuan dokumen tagihan dipenuhi.</p>
    <br>
    <table width="100%" border="0">
        <tr>
            <td width="60%"></td>
            <td>
                Kami Menyetujui segala kententuan yang tercantum pada Kwitansi yang kami terima. <br><br> 
                Hormat Kami, <br> 
                Bahagia23
                <br><br><br><br><br>
                
                <p>Manager</p>
            </td>
        </tr>
    </table>
</body>
</html>