<html>
<head>
    <title><?= $pageTitle ?></title>
</head>
<body>
    <h2 style="text-align:center;font-weight:bold;">Surat Penyerahan Bpkb</h2>
    <p>Yang bertanda tangan dibawah ini :</p>
    <table cellpadding="5" cellspacing="0">
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td><?= $thisData->Nm_Pengambil ?></td>
        </tr>
        <tr>
            <td>Nomor KTP</td>
            <td>:</td>
            <td><?= $thisData->No_Ktp_Pengambil ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?= $thisData->Alamat_Pengambil ?></td>
        </tr>
    </table>
    <br>
    <p>Dengan ini kami nyatakan bahwa B.P.K.B Kendaraan dengan informasi sebagai berikut :</p>
    <table cellpadding="5" cellspacing="0">
        <tr>
            <td>B.P.K.B atas nama</td>
            <td>:</td>
            <td><?= $thisData->Nm_Bpkb ?></td>
        </tr>
        <tr>
            <td>Nomor Rangka</td>
            <td>:</td>
            <td><?= $thisData->No_Rangka ?></td>
        </tr>
        <tr>
            <td>Nomor Mesin</td>
            <td>:</td>
            <td><?= $thisData->No_Mesin ?></td>
        </tr>
    </table>

    <br>
    <p>Akan kami serahkan ke <b><?= $thisData->Nm_Pengambil ?></b>, Alamat: <?= $thisData->Alamat_Pengambil ?>, dan tidak kepada pihak lain selambat-lambanya 3 (tiga) bulan sejak tanggal surat ini dikeluarkan.</p>
    <p>Demikianlah Surat ini dibuat untuk dipergunakan seperlunya.</p>
    <table border="0">
        <tr>
            <td>
                Pekan Baru, <?= date('d-m-Y') ?>
                <br><br><br><br><br>
                
                <p><?= nameSite ?><br>Manager</p>
            </td>
        </tr>
    </table>
    <p>Catatan :</p>
        <ol>
            <li>BPKB tersebut lengkap dengan faktur serta fotocopy STNK</li>
            <li>Segala perubahan yang menyangkut BPKB harus seizin <?= nameSite ?></li>
        </ol>
</body>
</html>