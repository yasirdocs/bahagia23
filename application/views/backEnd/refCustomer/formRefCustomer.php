<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <h4><?= $pageTitle ?></h4>
            <br>
            <div class="box box-primary">
                <form id="formBeli" action="<?= base_url("backEnd/RefUnitController/cetakKasir") ?>" method="post" target="__blank">
                    <div class="box-body">
                        <input type="hidden" value="<?= isset($thisData) ? $thisData->Kd_Unit : null ?>" name="Kd_Unit">
                        <div class="form-group">
                            <label id="label_1">Nomor Polisi</label>
                            <?php if(isset($inputStatus) ? $inputStatus : 0 != 0){ ?>
                                <input type="text" name="No_Plat" class="form-control" value="<?= isset($thisData) ? $thisData->No_Plat : null ?>" maxlength="155" readonly>  
                            <?php }else{ ?>
                                <select id="select_1" name="No_Plat" class="form-control form-select">
                                    <option></option>
                                    <?php foreach ($dataUnit as $key => $value) { ?>
                                        <option <?php if(isset($thisData)){ if($thisData->No_Plat==$value->No_Plat){ echo 'selected'; } }  ?> value="<?= $value->No_Plat ?>"><?= isset($thisData) ? $value->No_Plat : $value->No_Plat  ?></option>
                                    <?php } ?>
                                </select>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label>Nilai Terjual</label>
                            <input type="text" name="Harga_Jual" placeholder="Nilai Terjual" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="24" id="Nilai_Terjual" required>  
                        </div>
                        <div class="form-group">
                            <label>Fee Broker Jual</label>
                            <input type="text" name="Fee_Broker_Jual" placeholder="Fee Broker Jual" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="24" id="Fee_Broker_Jual" required>  
                        </div>
                        <div class="form-group">
                            <label>Insentif</label>
                            <input type="text" name="Insentif" placeholder="Insentif" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="24" id="Insentif" required>  
                        </div>
                        <div class="form-group">
                            <label>Refund</label>
                            <input type="text" name="Refund" placeholder="Refund" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="24" id="Refund" required>  
                        </div>
                        <div class="form-group">
                            <label>Dp Pesanan</label>
                            <input type="text" name="Dp_Pesanan" placeholder="Dp_Pesanan" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="24" id="Dp_Pesanan" required readonly>  
                        </div>
                        <div class="form-group">
                            <label>Total Pelunasan</label>
                            <input type="text" name="Total_Pelunasan" placeholder="Total_Pelunasan" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="24" id="Total_Pelunasan" required readonly>  
                        </div>
                        <!-- <div class="form-group">
                            <label>Discount</label>
                            <input type="text" name="Discount" placeholder="Discount %" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="2" required>  
                        </div> -->
                        <!-- <div class="form-group">
                            <label>Tanggal Pembelian</label>
                            <input id="Tgl_Keluar" type="text" name="Tgl_Keluar" placeholder="Tanggal Pembelian Unit" class="form-control" value="<?= isset($thisData) ? null : null ?>" required>  
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="col-md-6"><br><br><br>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Total Modal</label>
                            <input type="text" class="form-control uang" id="Total_Modal" value="<?= isset($thisData) ? $thisData->Total_Modal : null?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Profit</label>
                            <input id="Profit" type="text" name="Profit" placeholder="Profit" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="25" required readonly>  
                        </div>
                        <div class="form-group">
                            <label>Total Profit</label>
                            <input id="Total_Profit" type="text" name="Total_Profit" placeholder="Total Profit" class="form-control uang" value="<?= isset($thisData) ? null : null ?>" maxlength="25" required readonly>  
                        </div>
                        <label>Data Customer :</label>
                        <hr style="margin-top:0px;margin-bottom:10px;">
                        <div class="form-group">
                            <label>Nama Customer</label>
                            <input type="text" name="Nm_Customer" placeholder="Nama Customer" class="form-control" value="<?= isset($thisData) ? null : null ?>" maxlength="155" required>  
                        </div>
                        <div class="form-group">
                            <label>Nomor Ktp</label>
                            <input type="text" name="No_Ktp" placeholder="Nomor Ktp" class="form-control" value="<?= isset($thisData) ? null : null ?>" maxlength="30" required>  
                        </div>
                        <div class="form-group">
                            <label>Nomor Handphone</label>
                            <input type="text" name="No_Hp" placeholder="Nomor Handphone" class="form-control" value="<?= isset($thisData) ? null : null ?>" maxlength="15" required>  
                        </div>
                        <div class="form-group">
                            <label>Alamat Customer</label>
                            <textarea class="form-control" name="Alamat_Customer" id="" rows="5" value="" maxlength="255" placeholder="Alamat Customer"><?= isset($thisData) ? null : '' ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <button type="submit" id="btnHiddenBeli" class="hidden" style="display:none;"></button>
        </form>
        <!-- <div class="box-footer"> -->
        <button id="btnBeliUnit" type="button" class="btn btn-success" data-redirect="<?= base_url('refUnit') ?>"><i class="fa fa-money"></i> Beli</button>
        <?php if(isset($thisData)){ ?>
            <a data-url="<?= base_url('refUnitView/'.$thisData->Kd_Unit.'') ?>" class="btn btn-info modalView" title="Lihat"><i class="fa fa-eye"></i> Status</a>
        <?php } ?>
        <!-- </div> -->
</section>
<?php $this->load->view('libJs'); ?>
<script type="text/javascript">

    $('#btnBeliUnit').click(function(){
        var url = $(this).attr('data-redirect');
        if($(".uang").val() == 0 || $("input[name='Nm_Customer'").val() == 0 || $("input[name='No_Ktp'").val() == 0 || $("input[name='No_Hp'").val() == 0 || $("textarea[name='Alamat_Customer'").val() == 0){
            swal("Gagal", "Maaf, Data masih kosong", "error");
            return false;
        }
        swal({title: 'Informasi',text: "Apakah Data yang dimasukkan sudah benar? Data yang sudah dimasukkan tidak dapat diubah",type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'Ya, Cetak'
        }).then(function (isConfirm) {
            if(isConfirm){
                swal({
                  title: 'Informasi',
                  text: 'Mohon tunggu, sedang mencetak data',
                  timer: 3000,
                  allowEscapeKey: false,
                  onOpen: () => {
                    $('#btnHiddenBeli').trigger('click');
                    swal.showLoading();
                    setTimeout(function(){
                        window.location.href = url;
                    },150)
                  }
                }).then((result) => {
                  if (result.dismiss === 'timer') {
                    alert('I was closed by the timer')
                  }
                }).catch(swal.noop);
            };
        }).catch(swal.noop); //then
    });

    $('#Tgl_Keluar').datetimepicker({
        locale: 'id', 
        format: 'YYYY-MM-DD', 
        widgetPositioning:{
            vertical: "bottom"
        } 
    });

    // Bind an event
    // $('#select_1').on('select2:select', function (e) { 
    //     alert('select event');
    // });

    $(document).on('keyup', '#Nilai_Terjual, #Fee_Broker_Jual, #Insentif, #Refund', function(){
        var Modal = $('input#Total_Modal').val();
        var Nilai_Terjual = $('#Nilai_Terjual').val();
        var Fee_Broker_Jual = $('#Fee_Broker_Jual').val();
        var Insentif = $('#Insentif').val();
        var penguranganNilai = parseInt(Nilai_Terjual) - parseInt(Modal);
        var hasilProfit = parseInt(penguranganNilai) - parseInt(Fee_Broker_Jual) - parseInt(Insentif);
        $('input#Profit').val(hasilProfit);

        var refund = $("#Refund").val();
        var summaryProfit = parseInt(refund) + parseInt(hasilProfit);
        $("input#Total_Profit").val(summaryProfit);
    });
</script>
<?php $this->load->view('footer'); ?>