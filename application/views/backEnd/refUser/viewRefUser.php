<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <h2><?= $pageTitle ?></h2>
    <?php flashMessage(); ?>
        <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <a href="<?= base_url('refUserAdd') ?>" class="btn btn-primary">
                        Tambah
                    </a>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th>Username</th>
                                        <th>Level Akses</th>
                                        <th class="col-md-2">Aksi</th>
                                    </tr>
                                    <!-- <tr>
                                        <th><input type="text" id='nama' class="form-control input-table" placeholder="Cari"></th>
                                        <th></th>
                                        <th></th>
                                    </tr> -->
                                </thead>
                                <tbody>
                                <?php foreach($rows as $row) { ?>
                                    <tr>
                                        <td><?= $row->username ?></td>
                                        <td><?= $row->level_akses ?></td>
                                        <td>
                                           <a href="<?= base_url('refUserUpdate/'.$row->Kd_User.'') ?>" class="btn bg-orange" title="Ubah">
                                                    <i class="fa fa-edit"></i>
                                            </a>
                                            <a data-url="<?= base_url('refUserDelete/'.$row->Kd_User.'') ?>" class="btn btn-danger alertHapus" title="Hapus">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('libJs'); ?>
<?php $this->load->view('footer'); ?>