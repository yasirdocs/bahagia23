    <?php $this->load->view('header'); ?>
    <?php $this->load->view('menu'); ?>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <h4><?= $pageTitle ?></h4>
                <br>
                <div class="box box-primary">
                    <form action="<?=  current_url() ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control" value="<?= isset($thisData) ? $thisData->username : null ?>" required>  
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" value="<?= isset($thisData) ? $thisData->password : null ?>" required>  
                            </div>
                            <div class="form-group">
                                <label>Level Akses</label>
                                <select name="Kd_Level_Akses" class="form-control">
                                    <option></option>
                                    <?php foreach ($dataAkses as $key => $value) { ?>
                                        <option <?php if(isset($thisData)){ if($thisData->Kd_Level_Akses==$value->Kd_Level_Akses){ echo 'selected'; } }  ?> value="<?= $value->Kd_Level_Akses ?>"><?= $value->level_akses ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- <div class="col-md-6">
                <h4>&nbsp;</h4>
                <br>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="" name="tgl_masuk" class="form-control dateTime" value="<?= isset($thisData) ? $thisData->tgl_masuk : null ?>" required>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <?php $this->load->view('libJs'); ?>
    <?php $this->load->view('footer'); ?>