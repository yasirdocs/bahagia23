<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <h2><?= $pageTitle ?></h2>
    <?php flashMessage(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <a href="<?= base_url('refCustomerAdd') ?>" class="btn btn-primary">
                        Pembayaran
                    </a>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th>Nama Customer</th>
                                        <th class="col-md-2">Aksi</th>
                                    </tr>
                                    <!-- <tr>
                                        <th><input type="text" id='nama' class="form-control input-table" placeholder="Cari"></th>
                                        <th></th>
                                    </tr> -->
                                </thead>
                                <tbody>
                                <?php if($rows){
                                    foreach($rows as $row) { 
                                        ?>
                                        <tr>
                                            <td><?= $row->Nm_Customer ?></td>
                                            <td class="col-md-2">
                                            <a style="margin:4px;font-size:18px;" href="<?= base_url('refCustomerCetak/'.$row->Kd_Customer.'') ?>" target="__blank" class="btn bg-olive" title="Cetak Pembayaran"><i class="fa fa-print"></i></a>
                                            <a style="margin:4px;font-size:18px;" href="<?= base_url('refCustomerUpdate/'.$row->Kd_Customer.'') ?>" class="btn bg-orange" title="Ubah"><i class="fa fa-edit"></i></a>
                                            <a style="margin:4px;font-size:18px;" href="<?= base_url('refCustomerVerifikasi/'.$row->Kd_Customer.'') ?>" class="btn bg-navy" title="Verifikasi Pembayaran"><i class="fa fa-check"></i></a>
                                            <a style="margin:4px;font-size:18px;" data-url="<?= base_url('refCustomerDelete/'.$row->Kd_Customer.'') ?>" class="btn btn-danger alertHapus" title="Hapus"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php }
                                }else{ 
                                    ?>
                                    <tr>
                                        <td colspan="2" style="text-align:center">Data Masih Kosong</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('libJs'); ?>
<?php $this->load->view('footer'); ?>