    <?php $this->load->view('header'); ?>
    <?php $this->load->view('menu'); ?>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <h4><?= $pageTitle ?></h4>
                <br>
                <div class="box box-primary">
                    <form action="<?=  current_url() ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Merk Unit</label>
                                <select name="Kd_Merk" class="form-control">
                                    <option>Pilih Nama Merk</option>
                                    <?php foreach ($dataMerk as $key => $value) { ?>
                                        <option <?php if(isset($thisData)){ if($thisData->Kd_Merk==$value->Kd_Merk){ echo 'selected'; } }  ?> value="<?= $value->Kd_Merk ?>"><?= $value->Nm_Merk ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Varian Unit</label>
                                <select name="Kd_Varian" class="form-control">
                                    <option>Pilih Nama Varian</option>
                                    <?php foreach ($dataVarian as $key => $value) { ?>
                                        <option <?php if(isset($thisData)){ if($thisData->Kd_Varian==$value->Kd_Varian){ echo 'selected'; } }  ?> value="<?= $value->Kd_Varian ?>"><?= $value->Nm_Varian ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Type</label>
                                <input type="text" name="Nm_Type" placeholder="Nama Type" class="form-control" value="<?= isset($thisData) ? $thisData->Nm_Type : null ?>" required>  
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- <div class="col-md-6">
                <h4>&nbsp;</h4>
                <br>
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="" name="tgl_masuk" class="form-control dateTime" value="<?= isset($thisData) ? $thisData->tgl_masuk : null ?>" required>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>

    <?php $this->load->view('libJs'); ?>
    <?php $this->load->view('footer'); ?>