<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <h2><?= $pageTitle ?></h2>
    <?php flashMessage(); ?>
        <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <a href="<?= base_url('refTypeAdd') ?>" class="btn btn-primary">
                        Tambah
                    </a>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th>Nama Merk Unit</th>
                                        <th>Nama Varian</th>
                                        <th>Nama Type</th>
                                        <th class="col-md-2">Aksi</th>
                                    </tr>
                                    <!-- <tr>
                                        <th></th>
                                        <th></th>
                                        <th><input type="text" id='nama' class="form-control input-table" placeholder="Cari"></th>
                                        <th></th>
                                    </tr> -->
                                </thead>
                                <tbody>
                                <?php if($rows){
                                    foreach($rows as $row) { ?>
                                        <tr>
                                            <td><?= $row->Nm_Merk ?></td>
                                            <td><?= $row->Nm_Varian ?></td>
                                            <td><?= $row->Nm_Type ?></td>
                                            <td>
                                               <a href="<?= base_url('refTypeUpdate/'.$row->Kd_Type.'') ?>" class="btn bg-orange" title="Ubah">
                                                        <i class="fa fa-edit"></i>
                                                </a>
                                                <a data-url="<?= base_url('refTypeDelete/'.$row->Kd_Type.'') ?>" class="btn btn-danger alertHapus" title="Hapus">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
                                    }else{ ?>
                                        <tr>
                                            <td colspan="4" style="text-align:center;">Data Masih Kosong</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('libJs'); ?>
<?php $this->load->view('footer'); ?>