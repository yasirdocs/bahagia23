<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <h2><?= $pageTitle ?></h2>
    <?php flashMessage(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <!-- <a onclick="filter()" class="btn btn-info">
                        Cetak
                    </a> -->
                    <!-- <a href="<?= base_url("refUnitKeluarExcel") ?>" class="btn btn-info" target="__blank">
                        <i class="fa fa-print"></i> Excel
                    </a> -->
                    
                    <a id="modalFilter" class="btn btn-info">
                        <i class="fa fa-print"></i> Excel
                    </a>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Unit</th>
                                            <th>Status</th>
                                            <th>Merk</th>
                                            <th>Varian</th>
                                            <th>Type</th>
                                            <th>Harga Beli</th>
                                            <th>Harga Pasar</th>
                                            <th>Tanggal Masuk</th>
                                            <th class="col-md-2">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($rows){
                                            foreach($rows as $row) { 
                                                if($row->Kd_Transaksi) { ?>
                                                <tr>
                                                    <td><?= $row->Nm_Unit ?></td>
                                                    <td><?= $row->Nm_Status ?></td>
                                                    <td><?= $row->Nm_Merk ?></td>
                                                    <td><?= $row->Nm_Varian ?></td>
                                                    <td><?= $row->Nm_Type ?></td>
                                                    <td><?= number_format($row->Harga_Beli,0,',','.') ?></td>
                                                    <td><?= number_format($row->Harga_Pasar,0,',','.') ?></td>
                                                    <td><?= convertDateTime($row->Tgl_Masuk, 'd-m-Y') ?></td>
                                                    <td>
                                                       <a data-url="<?= base_url('refUnitKeluarView/'.$row->Kd_Unit.'') ?>" class="btn btn-info modalUnit" title="Lihat"><i class="fa fa-eye"></i></a>
                                                       <a href="<?= base_url('refUnitKeluarCetak/'.$row->Kd_Unit.'') ?>" class="btn btn-primary" title="Cetak" target="__blank"><i class="fa fa-print"></i></a>
                                                   </td>
                                               </tr>
                                               <?php }
                                           }
                                       }else{ ?>
                                       <tr>
                                        <td colspan="6" style="text-align:center;">Data Masih Kosong</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<!-- modal -->
<div id="viewModal" class="modal fade viewModal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= $pageTitle ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <table class="table table-striped tableCustom tableModal">
                        </table>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="table gambars"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal -->
<div class="modal fade" id="modal_Filter" tabindex="-1" role="dialog" aria-labelledby="modalFilterLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Filter</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <!-- <form action="<?= base_url("backEnd/RefUnitController/tambahFilterPost") ?>" id='form_Filter'> -->
                    <form id='form_Filter'>
                        <div class="form-group">
                            <label>Dari Tanggal</label>
                            <input type="text" class="form-control" placeholder="Pilih Tanggal" name="Tanggal_Masuk" id="Tanggal_Masuk">
                        </div>
                        <div class="form-group">
                            <label>S/D Tanggal</label>
                            <input type="text" class="form-control" placeholder="Pilih Tanggal" name="Tanggal_Keluar" id="Tanggal_Keluar">
                        </div>
                        <!-- <button type="submit" id="btnCetakFilter" class="hidden" style="display:none;"></button> -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button onclick="saveFilter()" id="printFilter" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<?php $this->load->view('libJs'); ?>
<script type="text/javascript">
    $('#btnFilter').click(function() {
        var url = "<?php echo base_url('refUnitKeluarCetakLaporan') ?>";
        $.ajax({
            url: url,
            type: "POST",
            data: $('#formfilterDate').serialize(),
            dataType: "JSON",
            success: function(data) {
                // if(data.status == true){
                    swal('Berhasil','Data Tidak ditemukan.','success');
                // }else{
                    // swal('Oops...','Data Tidak ditemukan.','error');
                // }
            }
        }); //ajax
    });

    $('#cetakFilter').click(function() {
        alert('test');
    });

    $('#tgl_awal').datetimepicker({
        locale: 'id', 
        format: 'YYYY-MM-DD', 
        widgetPositioning:{
            vertical: "bottom"
        } 
    });

    $('#tgl_akhir').datetimepicker({
        locale: 'id', 
        format: 'YYYY-MM-DD', 
        widgetPositioning:{
            vertical: "bottom"
        } 
    });

    $('.modalUnit').click(function(){
        var url = $(this).attr('data-url');
        $('.viewModal').modal('show');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                if(data.status == true){
                    $('.tableCustom').html(data.html);
                    $('.gambars').html(data.gambarUnit);
                }else{
                    swal('Oops...','Data Tidak ditemukan.','error');
                }
            }
        }); //ajax
    }); //document click

    function filter(){
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#formfilterDate')[0].reset();
        $(".filterDate").modal("show");
    }

    $('#modalFilter').click(function(){
        $('#modal_Filter').modal('show');
        $('#form_Filter')[0].reset();
        
        $('#Tanggal_Masuk').datetimepicker({
            locale: 'id', format: 'YYYY-MM-DD'
        });

        $('#Tanggal_Keluar').datetimepicker({
            locale: 'id', format: 'YYYY-MM-DD'
        });
    });

    function saveFilter(){
        var url = "<?= base_url("backEnd/RefUnitKeluarController/tambahFilterPost") ?>";
        var urls = "<?= base_url("backEnd/RefUnitKeluarController/exportExcel/") ?>";
        $.ajax({
            url: url,
            type: "POST",
            data: $("#form_Filter").serialize(),
            dataType: "JSON",
            success: function(data) {
                window.location.href = urls+data.Tgl_Masuk+"/"+data.Tgl_Keluar;
                $("#modal_Filter").modal('hide');
            }
        }); //ajax
    }
</script>
<?php $this->load->view('footer'); ?>