<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <h2><?= $pageTitle ?></h2>
    <?php flashMessage(); ?>
        <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <a href="#" class="btn btn-info">
                        Cetak
                    </a>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-bordered table-striped dataTable">
                                <thead>
                                    <tr>
                                        <th>Nama Unit</th>
                                        <th>Nomor Bpkb</th>
                                        <th>Nama Bpkb</th>
                                        <th>Nomor Plat</th>
                                        <th>Nomor Rangka</th>
                                        <th>Nomor Mesin</th>
                                        <th>Alamat Bpkb</th>
                                        <th class="col-md-2">Aksi</th>
                                    </tr>
                                    <!-- <tr>
                                        <th></th>
                                        <th></th>
                                        <th><input Bpkb="text" id='nama' class="form-control input-table" placeholder="Cari"></th>
                                        <th></th>
                                    </tr> -->
                                </thead>
                                <tbody>
                                <?php if($rows){
                                    foreach($rows as $row) { 
                                        if($row->Nm_Pengambil != null && $row->No_Ktp_Pengambil != null && $row->Tgl_Penyerahan_Bpkb != null){ ?>
                                        <tr>
                                            <td><?= $row->Nm_Unit ?></td>
                                            <td><?= $row->No_Bpkb ?></td>
                                            <td><?= $row->Nm_Bpkb ?></td>
                                            <td><?= $row->No_Plat ?></td>
                                            <td><?= $row->No_Rangka ?></td>
                                            <td><?= $row->No_Mesin ?></td>
                                            <td><?= $row->Alamat_Bpkb ?></td>
                                            <td>
                                                <a style="margin:4px;font-size:18px;" class="btn bg-navy modalPengambilView" title="Verifikasi Bpkb" href="<?= base_url('refBpkbKeluarLaporan/'.$row->Kd_Bpkb) ?>" target="__blank"><i class="fa fa-print"></i></a>
                                            </td>
                                        </tr>
                                    <?php }
                                        }
                                    }else{ ?>
                                        <tr>
                                            <td colspan="4" style="text-align:center;">Data Masih Kosong</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('libJs'); ?>
<script type="text/javascript">
    $('#verif').click(function(){
        alert('test');
    });
</script>
<?php $this->load->view('footer'); ?>