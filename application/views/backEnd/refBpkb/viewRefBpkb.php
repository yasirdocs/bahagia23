<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

<section class="content">
    <h2><?= $pageTitle ?></h2>
    <?php flashMessage(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <a href="<?= base_url('refBpkbAdd') ?>" class="btn btn-primary">
                        Tambah
                    </a>
                </div>
                <div class="box-body">
                    <div class="box">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped dataTable">
                                    <thead>
                                        <tr>
                                            <th>Nama Unit</th>
                                            <th>Nomor Bpkb</th>
                                            <th>Nama Pemilik Bpkb</th>
                                            <th>Nomor Plat</th>
                                            <th>Nomor Rangka</th>
                                            <th>Nomor Mesin</th>
                                            <th>Alamat Bpkb</th>
                                            <th class="col-md-2">Aksi</th>
                                        </tr>
                                    <!-- <tr>
                                        <th></th>
                                        <th></th>
                                        <th><input Bpkb="text" id='nama' class="form-control input-table" placeholder="Cari"></th>
                                        <th></th>
                                    </tr> -->
                                </thead>
                                <tbody>
                                    <?php if($rows){
                                        foreach($rows as $row) { 
                                            if($row->Nm_Pengambil == null && $row->No_Ktp_Pengambil == null && $row->Tgl_Penyerahan_Bpkb == null){ ?>
                                            <tr>
                                                <td><?= $row->Nm_Unit ?></td>
                                                <td><?= $row->No_Bpkb ?></td>
                                                <td><?= $row->Nm_Bpkb ?></td>
                                                <td><?= $row->No_Plat ?></td>
                                                <td><?= $row->No_Rangka ?></td>
                                                <td><?= $row->No_Mesin ?></td>
                                                <td><?= $row->Alamat_Bpkb ?></td>
                                                <td>
                                                    <a style="margin:4px;font-size:18px;" class="btn bg-navy modalPengambilView" title="Verifikasi Bpkb" data-kode="<?= $row->Kd_Bpkb ?>"><i class="fa fa-print"></i></a>
                                                    <a style="margin:4px;font-size:18px;" href="<?= base_url('refBpkbUpdate/'.$row->Kd_Bpkb.'') ?>" class="btn bg-orange" title="Ubah"><i class="fa fa-edit"></i></a>
                                                    <a style="margin:4px;font-size:18px;" data-url="<?= base_url('refBpkbDelete/'.$row->Kd_Bpkb.'') ?>" class="btn btn-danger alertHapus" title="Hapus"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php }
                                        }
                                    }else{ ?>
                                    <tr>
                                        <td colspan="4" style="text-align:center;">Data Masih Kosong</td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>



<!-- modal -->
<div class="modal fade" id="modal_Pengambil" tabindex="-1" role="dialog" aria-labelledby="modalPengambilLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Input Pengambil</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form action="<?= base_url("backEnd/RefBpkbController/tambahPengambilPost") ?>" id='form_Pengambil' method="POST" target="__blank">
                    <input type='hidden' value="" name='Kd_Bpkb'>
                    <div class='form-group'>
                        <label>Nama Pengambil</label>
                        <input type='text' name='Nm_Pengambil' placeholder='Nama Pengambil' class='form-control' value='' required>
                        <span class='help-block'></span>  
                    </div>
                    <div class='form-group'>
                        <label>Nomor Ktp Pengambil</label>
                        <input type='text' name='No_Ktp_Pengambil' placeholder='Nomor Ktp Pengambil' class='form-control' required>  
                        <span class='help-block'></span>  
                    </div>
                    <div class='form-group'>
                        <label>Alamat Pengambil</label>
                        <textarea class='form-control' name='Alamat_Pengambil' maxlength='255' placeholder='Alamat Pengambil'></textarea>
                        <span class='help-block'></span>  
                    </div>
                    <button type="submit" id="btnCetakPengambil" class="hidden" style="display:none;"></button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button onclick="save()" id="printPengambil" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<?php $this->load->view('libJs'); ?>
<script type="text/javascript">

    $('.modalPengambilView').click(function(){
        var Kd_Bpkb = $(this).attr("data-kode");
        $("input[name='Kd_Bpkb']").val(Kd_Bpkb);
        
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#form_Pengambil')[0].reset();
        $("#modal_Pengambil").modal("show");
    });

    $("#btnCetakPengambil").click(function(){
        swal("berhasil", "data berhasil", "success");
        $("#modal_Pengambil").modal("hide");
        setTimeout(function(){
            window.location.reload();
        },100)
    });

    function save(){
        if($("input[name='Nm_Pengambil']").val() == 0 || $("input[name='No_Ktp_Pengambil']").val() == 0 ||  $("textarea[name='Alamat_Pengambil']").val() == 0){
            swal("Gagal", "Maaf, Data Pengambil masih kosong", "error");
            return false;
        }
        $("#btnCetakPengambil").trigger("click");
    }

</script>
<?php $this->load->view('footer'); ?>