<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>

    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <h4><?= $pageTitle ?></h4>
                <br>
                <div class="box box-primary">
                    <form action="<?=  current_url() ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label id="label_1">Nama Unit</label>
                                <select id="select_1" name="Kd_Unit" class="form-control">
                                    <option></option>
                                    <?php foreach ($dataUnit as $key => $value) { ?>
                                    <option <?php if(isset($thisData)){ if($thisData->Kd_Unit==$value->Kd_Unit){ echo 'selected'; } }  ?> value="<?= $value->Kd_Unit ?>"><?= $value->Nm_Unit ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label id="label_2">Status Bpkb</label>
                                <select id="select_2" name="Kd_Status_Bpkb" class="form-control">
                                    <option></option>
                                    <?php foreach ($dataStatusBpkb as $key => $value) { ?>
                                    <option <?php if(isset($thisData)){ if($thisData->Kd_Status_Bpkb==$value->Kd_Status_Bpkb){ echo 'selected'; } }  ?> value="<?= $value->Kd_Status_Bpkb ?>"><?= $value->Nm_Status_Bpkb ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nama Pemilik Bpkb</label>
                                <input type="text" name="Nm_Bpkb" placeholder="Nama Pemilik Bpkb" class="form-control" value="<?= isset($thisData) ? $thisData->Nm_Bpkb : null ?>" required>  
                            </div>
                            <div class="form-group">
                                <label>Keterangan Bpkb</label>
                                <textarea class="form-control" name="Keterangan_Bpkb" id="" rows="4" value="" maxlength="255" placeholder="Keterangan Bpkb"><?= isset($thisData) ? $thisData->Keterangan_Bpkb : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nomor Bpkb</label>
                                <input type="text" name="No_Bpkb" placeholder="Nomor Bpkb" class="form-control" value="<?= isset($thisData) ? $thisData->No_Bpkb : null ?>" required>  
                            </div>
                            <div class="form-group">
                                <label>Nomor Rangka</label>
                                <input type="text" name="No_Rangka" placeholder="Nomor Rangka" class="form-control" value="<?= isset($thisData) ? $thisData->No_Rangka : null ?>" required>  
                            </div>
                            <div class="form-group">
                                <label>Nomor Mesin</label>
                                <input type="text" name="No_Mesin" placeholder="Nomor Mesin" class="form-control" value="<?= isset($thisData) ? $thisData->No_Mesin : null ?>" required>  
                            </div>
                            <div class="form-group">
                                <label>Alamat Bpkb</label>
                                <textarea class="form-control" name="Alamat_Bpkb" id="" rows="4" value="" maxlength="255" placeholder="Alamat Bpkb"><?= isset($thisData) ? $thisData->Alamat_Bpkb : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </section>

<?php $this->load->view('libJs'); ?>
<?php $this->load->view('footer'); ?>