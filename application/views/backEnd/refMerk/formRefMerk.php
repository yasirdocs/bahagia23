<?php $this->load->view('header'); ?>
<?php $this->load->view('menu'); ?>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <h4><?= $pageTitle ?></h4>
                <br>
                <div class="box box-primary">
                    <form action="<?=  current_url() ?>" method="post">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Nama Merk Unit</label>
                                <input type="text" placeholder="Nama Merk" name="Nm_Merk" class="form-control" value="<?= isset($thisData) ? $thisData->Nm_Merk : null ?>" required>  
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php $this->load->view('libJs'); ?>
<?php $this->load->view('footer'); ?>