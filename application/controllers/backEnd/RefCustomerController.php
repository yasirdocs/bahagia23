<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefCustomerController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Unit',
                'MRef_Bpkb',
                'MRef_Customer'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle']     = "Kasir";
        $paramater['rows']          = $this->MRef_Customer->getDataByQuery();
        $this->load->view('backEnd/refCustomer/viewRefCustomer',$paramater);
    }

    function add(){
        if(!isLogin()){
            redirect('login');
        }

        $pageTitle = "Kasir - Pembelian Unit";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->MRef_Customer->Kd_Unit               => $this->input->post('Kd_Unit'),
                $this->MRef_Customer->Kd_Bpkb               => $this->input->post('Kd_Bpkb'),
                $this->MRef_Customer->Nm_Customer           => $this->input->post('Nm_Customer'),
                $this->MRef_Customer->No_Ktp                => $this->input->post('No_Ktp'),
                $this->MRef_Customer->No_Hp                 => $this->input->post('No_Hp'),
                $this->MRef_Customer->Alamat_Customer       => $this->input->post('Alamat_Customer')
            );

            $this->MRef_Customer->insert($data);

            transStatus('Data ',0,null,'refCustomerAdd');
            transStatus('Data ',1,null,'refCustomer');
        }


        $paramater['pageTitle']     = $pageTitle;
        $paramater['dataUnit']      = $this->MRef_Unit->getAll();
        $paramater['dataBpkb']      = $this->MRef_Bpkb->getAll();

        $this->load->view('backEnd/refCustomer/formRefCustomer',$paramater);
    }
    
    function cetak($id){ //fungsi cetak struk
        if(!isLogin()){
            redirect('login');
        }
        $pdfFilePath                   = strtoupper("Cetak Struk").".pdf";
        $paramater['pageTitle']        = $pdfFilePath;


        $this->load->library('M_pdf');
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4-L',
        ]);

        $html = $this->load->view('backEnd/pdf/strukPembayaran',$paramater,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, "I");
    }

    function beli($id){
        if(!isLogin()){
            redirect('login');
        }
        
        $dataUnit = $this->MRef_Unit->getDataByQuery(array('Kd_Unit'),array($id),'row',array());

        $pageTitle = "Kasir - Pembelian Unit";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->MRef_Customer->Kd_Unit               => $this->input->post('Kd_Unit'),
                $this->MRef_Customer->Kd_Bpkb               => $this->input->post('Kd_Bpkb'),
                $this->MRef_Customer->Nm_Customer           => $this->input->post('Nm_Customer'),
                $this->MRef_Customer->No_Ktp                => $this->input->post('No_Ktp'),
                $this->MRef_Customer->No_Hp                 => $this->input->post('No_Hp'),
                $this->MRef_Customer->Alamat_Customer       => $this->input->post('Alamat_Customer')
            );

            $this->MRef_Customer->insert($data);

            transStatus('Data ',0,null,'refCustomerAdd');
            transStatus('Data ',1,null,'refCustomer');
        }
        
        $pdfFilePath                   = strtoupper("Cetak Struk").".pdf";
        $paramater['pageTitle']        = $pdfFilePath;


        $this->load->library('M_pdf');
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4-L',
        ]);

        $html = $this->load->view('backEnd/pdf/strukPembayaran',$paramater,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, "I");

        $paramater['pageTitle']     = $pageTitle;
        $paramater['thisData']      = $dataUnit;
        $paramater['dataUnit']      = $this->MRef_Unit->getAll();
        $paramater['dataBpkb']      = $this->MRef_Bpkb->getAll();

        $this->load->view('backEnd/refCustomer/formRefCustomer',$paramater);
    }


}