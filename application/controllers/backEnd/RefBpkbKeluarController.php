<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefBpkbKeluarController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk',
                'MRef_Unit',
                'MRef_Bpkb',
                'MRef_Bpkb_Keluar'
            ));
    }

    function index(){   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar Bpkb Keluar";
        $paramater['rows']      = $this->MRef_Bpkb->getDataByQuery();

        $this->load->view('backEnd/refBpkbKeluar/viewRefBpkbKeluar',$paramater);
    }

    function laporan($id){
        if(!isLogin()){
            redirect('login');
        }

        $dataUnit = $this->MRef_Bpkb->getDataBy(array('Kd_Bpkb'),array($id),'row',array());
        $pdfFilePath                   = strtoupper("Cetak BPKBKeluar").".pdf";
        $paramater['pageTitle']        = $pdfFilePath;
        $paramater['dataUnit']         = $dataUnit;


        $this->load->library('M_pdf');
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4-L',
        ]);

        $html = $this->load->view('backEnd/pdf/viewLaporanBpkbKeluar',$paramater,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, "I");
    }
}