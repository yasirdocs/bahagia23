<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefUserController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'Map_user',
                'Map_user_akses'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar User";
        $paramater['rows']      = $this->Map_user->getAkses();

        $this->load->view('backEnd/refUser/viewRefUser',$paramater);
    }

    function add(){
        $pageTitle = "Tambah User";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->Map_user->username              => $this->input->post('username'),
                $this->Map_user->password              => $this->input->post('password'),
                $this->Map_user->Kd_Level_Akses           => $this->input->post('Kd_Level_Akses')
            );

            $this->Map_user->insert($data);

            transStatus('data ',0,null,'refUserAdd');

            transStatus('data ',1,null,'refUser');
            
        }


        $paramater['pageTitle'] = $pageTitle;
        $paramater['dataAkses'] = $this->Map_user_akses->getAll();
        $this->load->view('backEnd/refUser/formRefUser',$paramater);
    }

    function update($id){
        $pageTitle = "Ubah User";

        $thisData = $this->Map_user->getDataBy(array('Kd_User'),array($id),'row');
        if(!empty($_POST)) {
            $kodeId         = $thisData->Kd_User;
            $field          = array('Kd_User' => $kodeId);

            $this->db->trans_begin();

            $data = array(
                $this->Map_user->username              => $this->input->post('username'),
                $this->Map_user->password              => $this->input->post('password'),
                $this->Map_user->Kd_Level_Akses           => $this->input->post('Kd_Level_Akses')
            );

            $this->Map_user->updateBy($data,$field);
            transStatus('data ',0,null,'refUserUpdate/'.$field);
            transStatus('data ',1,null,'refUser');
            
        }


        $paramater['pageTitle']  = $pageTitle;
        $paramater['thisData']   = $thisData;
        $paramater['dataAkses']  = $this->Map_user_akses->getAll();

        $this->load->view('backEnd/refUser/formRefUser',$paramater);
    }


    function delete($id){
        if(!isLogin()){
            redirect('login');
        }
        $this->Map_user->deleteDataBy(array('Kd_User' => $id));
        echo json_encode(array("status" => TRUE, "pesan" => "Data", "redirect" => "refUser"));
    }

}