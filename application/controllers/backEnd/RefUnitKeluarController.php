<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefUnitKeluarController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk',
                'MRef_Varian',
                'MRef_Type',
                'MRef_Unit'
            ));
    }

    function index(){   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar Unit Keluar";
        $paramater['rows']      = $this->MRef_Unit->getDataByQuery();

        $this->load->view('backEnd/refUnitKeluar/viewRefUnitKeluar',$paramater);
    }
    
    function view($id){   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Informasi Unit";
        $dataUnit = $this->MRef_Unit->getDataByQuery(array('Kd_Unit'),array($id),'row',array());
        $html = "<tr>       
                            <th>Tahun</th>
                            <td>: ".$dataUnit->Tahun."</td>
                        </tr><tr>
                            <th>Nama Unit</th>
                            <td>: ".$dataUnit->Nm_Unit."</td>
                        </tr><tr>
                            <th>Nomor Plat</th>
                            <td>: ".$dataUnit->No_Plat."</td>
                        </tr><tr>
                            <th>Status</th>
                            <td>: ".$dataUnit->Nm_Status."</td>
                        </tr><tr>
                            <th>Merk</th>
                            <td>: ".$dataUnit->Nm_Merk."</td>
                        </tr><tr>
                            <th>Varian</th>
                            <td>: ".$dataUnit->Nm_Varian."</td>
                        </tr><tr>
                            <th>Type</th>
                            <td>: ".$dataUnit->Nm_Type."</td>
                        </tr><tr>
                            <th>Tanggal Masuk</th>
                            <td>: ".convertDateTime($dataUnit->Tgl_Masuk, 'd-m-Y')."</td>
                        </tr><tr>
                            <th>Harga Pasar</th>
                            <td>: Rp. ".number_format($dataUnit->Harga_Pasar,0,',','.')."</td>
                        </tr><tr>
                            <th>Harga Beli</th>
                            <td>: Rp. ".number_format($dataUnit->Harga_Beli,0,',','.')."</td>
                        </tr><tr>
                            <th>Biaya Perawatan</th>
                            <td>: Rp. ".number_format($dataUnit->Biaya_Perawatan,0,',','.')."</td>
                        </tr><tr>
                            <th>Biaya Broker</th>
                            <td>: Rp. ".number_format($dataUnit->Biaya_Broker,0,',','.')."</td>
                        </tr><tr>
                            <th>Total Modal</th>
                            <td>: Rp. ".number_format($dataUnit->Total_Modal,0,',','.')."</td>
                        </tr>
                        
            ";
        $gambar = "<img class='img-responsive' src='".assetsImages.$dataUnit->Foto_Unit."?>'>";
        echo json_encode(array("status" => true, "html" => $html, "gambarUnit" => $gambar));
    }

    function cetakUlang($id){
        $thisData       = $this->MRef_Unit->getDataByQuery(array('ru.Kd_Unit'),array($id),'row');
        $kodeId         = $thisData->Kd_Unit;
        $field          = array('Kd_Unit' => $kodeId);
    
        $pdfFilePath                    = strtoupper("Cetak Unit").".pdf";
        $paramater['pageTitle']         = $pdfFilePath;
        $paramater['thisData']          = $thisData;

        $this->load->library('M_pdf');
        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8', 'format' => [148, 210] //format mm 160==16cm
        ]);

        $html = $this->load->view('backEnd/pdf/strukPembayaran',$paramater,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, "I");
    }

    function tambahFilterPost(){
        $Tgl_Masuk  = $this->input->post("Tanggal_Masuk");
        $Tgl_Keluar = $this->input->post("Tanggal_Keluar");

        echo json_encode(array("status" => true, "Tgl_Masuk" => $Tgl_Masuk, "Tgl_Keluar" => $Tgl_Keluar));
    }

    function exportExcel($Tgl_Masuk=null,$Tgl_Keluar=null){
        
        $pageTitle = "Laporan Unit Keluar";
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);//activate worksheet number 1
        $this->excel->getActiveSheet()->setTitle('worksheet');//name the worksheet
        $formatHarga = '#,##0';
        $styleBorder = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );

        //set cell A1 content with some text
        $this->excel->getActiveSheet()
            ->setCellValue('A3', 'LAPORAN PENJUALAN UNIT')
            ->setCellValue('A5', 'NO')
            ->setCellValue('B5', 'NAMA PEMBELI')
            ->setCellValue('C5', 'ALAMAT')
            ->setCellValue('D5', 'NO KTP')
            ->setCellValue('E5', 'NO HP')
            ->setCellValue('F5', 'MERK')
            ->setCellValue('G5', 'VARIAN')
            ->setCellValue('H5', 'TIPE')
            ->setCellValue('I5', 'TAHUN')
            ->setCellValue('J5', 'NO PLAT')
            ->setCellValue('K5', 'HARGA')
            ->setCellValue('L5', 'TANGGAL PEMBELIAN')
        ;

        $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(14);//change the font size
        $cell = array('A3','A5','B5','C5','D5','E5','F5','G5','H5','I5','J5','K5','L5','M5','N5');
        foreach ($cell as $key => $value) {
            $column = substr($value,0,1);
            $this->excel->getActiveSheet()->getStyle($value)->getFont()->setBold(true);//make the font become bold
            $this->excel->getActiveSheet()->getStyle($value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
        }


        if(!$Tgl_Masuk && !$Tgl_Keluar){
            $dataUnit = $this->MRef_Unit->getQueryRight();
        }else{
            $dataFilter = $this->MRef_Unit->getQueryUnit('right')->where(["Tgl_Pembelian >=" => $Tgl_Masuk." 00:00:00", "Tgl_Pembelian <" => $Tgl_Keluar." 23:59:59"])->get()->result();
            $dataUnit = $dataFilter;
        }

        $countDataUnit = count($dataUnit);
        if($dataUnit){
            foreach ($dataUnit as $key => $value) {
        // for ($char = 'A'; $char <= 'N'; $char++) { //LOOPING VARCHAR A TO N
                $keyd=$key+6;
                $this->excel->getActiveSheet()->setCellValue('A'.$keyd,$key+1)
                    ->setCellValue('B'.$keyd,$value->Nm_Pembeli)
                    ->setCellValue('C'.$keyd,$value->Alamat_Pembeli)
                    ->setCellValue('D'.$keyd,$value->No_Ktp)
                    ->setCellValue('E'.$keyd,$value->No_Hp)
                    ->setCellValue('F'.$keyd,$value->Nm_Merk)
                    ->setCellValue('G'.$keyd,$value->Nm_Varian)
                    ->setCellValue('H'.$keyd,$value->Nm_Type)
                    ->setCellValue('I'.$keyd,$value->Tahun)
                    ->setCellValue('J'.$keyd,$value->No_Plat)
                    ->setCellValue('K'.$keyd,$value->Nilai_Terjual)
                    ->setCellValue('L'.$keyd,$value->Tgl_Pembelian);


                $this->excel->getActiveSheet()->getStyle('A'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('B'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('C'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('D'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('E'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('F'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('G'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('H'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('I'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('J'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('K'.$keyd)->applyFromArray($styleBorder)->getNumberFormat()->setFormatCode($formatHarga);;
                $this->excel->getActiveSheet()->getStyle('L'.$keyd)->applyFromArray($styleBorder);

                // $this->excel->getActiveSheet()->setCellValue('K'.++$keyd,'=IF(K'.--$keyd.'>1000,"PROFIT","PENDING")'); LOGIC IF
                $this->excel->getActiveSheet()->setCellValue('K'.++$keyd,'=SUM(K6,K'.--$keyd.')');
                $this->excel->getActiveSheet()->getStyle('K'.++$keyd)->applyFromArray($styleBorder)->getNumberFormat()->setFormatCode($formatHarga);;

                $this->excel->getActiveSheet()->setCellValue('J'.$keyd,'TOTAL');
                $this->excel->getActiveSheet()->getStyle('J'.$keyd)->applyFromArray($styleBorder);
            }
        }else{
            $this->excel->getActiveSheet()->setCellValue('A6','Data Kosong');
        }

        $this->excel->getActiveSheet()->mergeCells('A3:L3'); //merge cell
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //set aligment to center         
        $this->excel->getActiveSheet()->getStyle('A5:L5')->applyFromArray($styleBorder);

        $filename=$pageTitle.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }


}