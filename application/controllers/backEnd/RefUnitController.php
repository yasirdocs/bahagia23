<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefUnitController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk',
                'MRef_Varian',
                'MRef_Type',
                'MRef_Unit',
                'MRef_Customer',
                'Ta_Transaksi',
                'MRef_Pesanan'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Stok Unit";
        $paramater['rows']      = $this->MRef_Unit->getDataByQuery();

        $this->load->view('backEnd/refUnit/viewRefUnit',$paramater);
    }
    
    function view($id){   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Informasi Unit";
        $dataUnit = $this->MRef_Unit->getDataByQuery(array('Kd_Unit'),array($id),'row',array());
        $html = "<tr>       
                            <th>Tahun</th>
                            <td>: ".$dataUnit->Tahun."</td>
                        </tr><tr>
                            <th>Nama Unit</th>
                            <td>: ".$dataUnit->Nm_Unit."</td>
                        </tr><tr>
                            <th>Status</th>
                            <td>: ".$dataUnit->Nm_Status."</td>
                        </tr><tr>
                            <th>Merk</th>
                            <td>: ".$dataUnit->Nm_Merk."</td>
                        </tr><tr>
                            <th>Varian</th>
                            <td>: ".$dataUnit->Nm_Varian."</td>
                        </tr><tr>
                            <th>Type</th>
                            <td>: ".$dataUnit->Nm_Type."</td>
                        </tr><tr>
                            <th>No Rangka</th>
                            <td>: ".$dataUnit->No_Rangka."</td>
                        </tr><tr>
                            <th>No Mesin</th>
                            <td>: ".$dataUnit->No_Mesin."</td>
                        </tr><tr>
                            <th>Tanggal Masuk</th>
                            <td>: ".convertDateTime($dataUnit->Tgl_Masuk, 'd-m-Y')."</td>
                        </tr><tr>
                            <th>Harga Pasar</th>
                            <td>: Rp. ".number_format($dataUnit->Harga_Pasar,0,',','.')."</td>
                        </tr><tr>
                            <th>Harga Beli</th>
                            <td>: Rp. ".number_format($dataUnit->Harga_Beli,0,',','.')."</td>
                        </tr><tr>
                            <th>Biaya Perawatan</th>
                            <td>: Rp. ".number_format($dataUnit->Biaya_Perawatan,0,',','.')."</td>
                        </tr><tr>
                            <th>Biaya Broker</th>
                            <td>: Rp. ".number_format($dataUnit->Biaya_Broker,0,',','.')."</td>
                        </tr><tr>
                            <th>Total Modal</th>
                            <td>: Rp. ".number_format($dataUnit->Total_Modal,0,',','.')."</td>
                        </tr>
                        
            ";
        $gambar = "<img class='img-responsive' src='".assetsImages.$dataUnit->Foto_Unit."?>'>";
        echo json_encode(array("status" => true, "html" => $html, "gambarUnit" => $gambar));
    }

    function add(){
        if(!isLogin()){
            redirect('login');
        }

        $pageTitle = "Tambah Unit";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $config['upload_path']          = dirname(APPPATH).'/assets/images/unit/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['maintain_ratio']       = TRUE;
            $config['width']                = 1400;
            $config['height']               = 1400;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('Foto_Unit')){
                $Foto_Unit = $this->upload->data()['file_name'];
            }

            $Harga_Beli = convertMoney($this->input->post('Harga_Beli'));
            $Biaya_Perawatan = convertMoney($this->input->post('Biaya_Perawatan'));
            $Biaya_Broker = convertMoney($this->input->post('Biaya_Broker'));
            $Total_Modal = (int)$Harga_Beli + (int)$Biaya_Perawatan + (int)$Biaya_Broker;

            $data = array(
                $this->MRef_Unit->Tahun              => $this->input->post('Tahun'),
                $this->MRef_Unit->Nm_Unit            => $this->input->post('Nm_Unit'),
                $this->MRef_Unit->No_Plat            => $this->input->post('No_Plat'),
                $this->MRef_Unit->No_Rangka          => $this->input->post('No_Rangka'),
                $this->MRef_Unit->No_Mesin           => $this->input->post('No_Mesin'),
                $this->MRef_Unit->Kd_Status          => 1,
                $this->MRef_Unit->Kd_Merk            => $this->input->post('Kd_Merk'),
                $this->MRef_Unit->Kd_Varian          => $this->input->post('Kd_Varian'),
                $this->MRef_Unit->Kd_Type            => $this->input->post('Kd_Type'),
                $this->MRef_Unit->Harga_Beli         => $Harga_Beli,
                $this->MRef_Unit->Biaya_Perawatan    => $Biaya_Perawatan,
                $this->MRef_Unit->Biaya_Broker       => $Biaya_Broker,
                $this->MRef_Unit->Total_Modal        => $Total_Modal,
                $this->MRef_Unit->Foto_Unit          => $Foto_Unit,
                $this->MRef_Unit->Harga_Pasar        => convertMoney($this->input->post('Harga_Pasar')),
                $this->MRef_Unit->Tgl_Masuk          => date('Y-m-d')
            );

            $this->MRef_Unit->insert($data);

            transStatus('Data ',0,null,'refUnitAdd');
            transStatus('Data ',1,null,'refUnit');
        }


        $paramater['dataMerk']      = $this->MRef_Merk->getAll();
        $paramater['dataVarian']    = $this->MRef_Varian->getAll();
        $paramater['dataType']      = $this->MRef_Type->getAll();
        $paramater['pageTitle']     = $pageTitle;
        $this->load->view('backEnd/refUnit/formRefUnit',$paramater);
    }

    function update($id){
        if(!isLogin()){
            redirect('login');
        }
        $pageTitle = "Ubah Unit";

        $thisData = $this->MRef_Unit->getDataBy(array('Kd_Unit'),array($id),'row');
        if(!empty($_POST)) {
            $kodeId         = $thisData->Kd_Unit;
            $field          = array('Kd_Unit' => $kodeId);

            $this->db->trans_begin();
            
            $config['upload_path']          = dirname(APPPATH).'/assets/images/unit/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['maintain_ratio']       = TRUE;
            $config['width']                = 1400;
            $config['height']               = 1400;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('Foto_Unit')){
                $Foto_Unit = $this->upload->data()['file_name'];
            }

            $Harga_Beli = convertMoney($this->input->post('Harga_Beli'));
            $Biaya_Perawatan = convertMoney($this->input->post('Biaya_Perawatan'));
            $Biaya_Broker = convertMoney($this->input->post('Biaya_Broker'));
            $Total_Modal = (int)$Harga_Beli + (int)$Biaya_Perawatan + (int)$Biaya_Broker;

            $data = array(
                $this->MRef_Unit->Tahun              => $this->input->post('Tahun'),
                $this->MRef_Unit->Nm_Unit            => $this->input->post('Nm_Unit'),
                $this->MRef_Unit->No_Plat            => $this->input->post('No_Plat'),
                $this->MRef_Unit->No_Rangka          => $this->input->post('No_Rangka'),
                $this->MRef_Unit->No_Mesin           => $this->input->post('No_Mesin'),
                $this->MRef_Unit->Kd_Status          => 1,
                $this->MRef_Unit->Kd_Merk            => $this->input->post('Kd_Merk'),
                $this->MRef_Unit->Kd_Varian          => $this->input->post('Kd_Varian'),
                $this->MRef_Unit->Kd_Type            => $this->input->post('Kd_Type'),
                $this->MRef_Unit->Harga_Beli         => $Harga_Beli,
                $this->MRef_Unit->Biaya_Perawatan    => $Biaya_Perawatan,
                $this->MRef_Unit->Biaya_Broker       => $Biaya_Broker,
                $this->MRef_Unit->Total_Modal        => $Total_Modal,
                $this->MRef_Unit->Foto_Unit          => $Foto_Unit,
                $this->MRef_Unit->Harga_Pasar        => convertMoney($this->input->post('Harga_Pasar')),
                $this->MRef_Unit->Tgl_Masuk          => date('Y-m-d')
            );

            $this->MRef_Unit->updateBy($data,$field);
            transStatus('Data ',0,null,'refUnitUpdate/'.$field);
            transStatus('Data ',1,null,'refUnit');   
        }
        
        $paramater['dataMerk']      = $this->MRef_Merk->getAll();
        $paramater['dataVarian']    = $this->MRef_Varian->getAll();
        $paramater['dataType']      = $this->MRef_Type->getAll();
        $paramater['pageTitle']  = $pageTitle;
        $paramater['thisData']   = $thisData;

        $this->load->view('backEnd/refUnit/formRefUnit',$paramater);
    }

    function delete($id){
        if(!isLogin()){
            redirect('login');
        }
        $this->MRef_Unit->deleteDataBy(array('Kd_Unit' => $id ));
        echo json_encode(array("status" => TRUE, "pesan" => "Data", "redirect" => "refUnit"));
    }

    function kasir($id){
        if(!isLogin()){
            redirect('login');
        }
        
        $pageTitle = "Beli Unit";
        $thisData = $this->MRef_Unit->getDataBy(array('Kd_Unit'),array($id),'row');

        $paramater['dataUnit']      = $this->MRef_Unit->getAll();
        $paramater['dataMerk']      = $this->MRef_Merk->getAll();
        $paramater['dataVarian']    = $this->MRef_Varian->getAll();
        $paramater['dataType']      = $this->MRef_Type->getAll();
        $paramater['pageTitle']     = $pageTitle;
        $paramater['thisData']      = $thisData;
        $paramater['inputStatus']   = 1;

        $this->load->view('backEnd/refCustomer/formRefCustomer',$paramater);
    }

    function cetakKasir(){
        if(!empty($_POST)){
            $id             = $this->input->post("Kd_Unit");
            $thisData       = $this->MRef_Unit->getDataBy(array('Kd_Unit'),array($id),'row');
            $kodeId         = $thisData->Kd_Unit;
            $field          = array('Kd_Unit' => $kodeId);

            $this->db->trans_begin();
            $data = array(
                $this->Ta_Transaksi->Nilai_Terjual            => convertMoney($this->input->post('Harga_Jual')),
                $this->Ta_Transaksi->Fee_Broker_Jual          => convertMoney($this->input->post('Fee_Broker_Jual')),
                $this->Ta_Transaksi->Insentif                 => convertMoney($this->input->post('Insentif')),
                $this->Ta_Transaksi->Refund                   => convertMoney($this->input->post('Refund')),
                $this->Ta_Transaksi->Profit                   => convertMoney($this->input->post('Profit')),
                $this->Ta_Transaksi->Total_Profit             => convertMoney($this->input->post('Total_Profit')),
                $this->Ta_Transaksi->Nm_Pembeli               => $this->input->post('Nm_Customer'),
                $this->Ta_Transaksi->No_Hp                    => $this->input->post('No_Hp'),
                $this->Ta_Transaksi->Alamat_Pembeli           => $this->input->post('Alamat_Customer'),
                $this->Ta_Transaksi->No_Ktp                   => $this->input->post('No_Ktp'),
                $this->Ta_Transaksi->Tgl_Pembelian            => date('Y-m-d')
            );

            $this->Ta_Transaksi->insert($data);
            $Kd_Transaksi = $this->db->insert_id();
            $data = array(
                $this->MRef_Unit->Kd_Transaksi                => $Kd_Transaksi,
                $this->MRef_Unit->Tgl_Keluar                  => date('Y-m-d')
            );
            $this->MRef_Unit->updateBy($data, $field);
            
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $pesan="Maaf, Data gagal disimpan";
                $CI->session->set_flashdata('success',$pesan);
                redirect('/dashboard');
            }else{
                $this->db->trans_commit();
            
                $thisData                       = $this->Ta_Transaksi->getDataByQuery(array('tt.Kd_Transaksi'),array($Kd_Transaksi),'row');
                $pdfFilePath                    = strtoupper("Cetak Unit").".pdf";
                $paramater['pageTitle']         = $pdfFilePath;
                $paramater['thisData']          = $thisData;

                $this->load->library('M_pdf');
                $mpdf = new \Mpdf\Mpdf([
                    'mode' => 'utf-8', 'format' => [148, 210] //format mm 160==16cm
                ]);

                $html = $this->load->view('backEnd/pdf/strukPembayaran',$paramater,true);
                $mpdf->WriteHTML($html);
                $mpdf->Output($pdfFilePath, "I");        
            }
        }else{
            exit("Maaf, Anda tidak punya akses disini");
        }
    }

    function pesan(){
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Stok Booking Unit";
        $paramater['rows']      = $this->MRef_Unit->getDataByQuery();

        // var_dump($this->db->last_query());
        // var_dump($paramater['rows']);exit;
        $this->load->view('backEnd/refUnitPesanan/viewRefUnitPesanan',$paramater);
    }

    function tambahPesananPost(){
        $Kd_Unit        = $this->input->post('Kd_Unit');
        $this->db->trans_begin();

        $data = array(
            $this->MRef_Pesanan->Dp_Pesanan            => $this->input->post('dp_pesanan'),
            $this->MRef_Pesanan->Total_Bayar           => $this->input->post('total_bayar'),
            $this->MRef_Pesanan->Tgl_Pelunasan         => $this->input->post('Tanggal_Pelunasan')
        );

        $this->MRef_Pesanan->insert($data);
        $dataPesanan = $this->db->insert_id();

        $field          = array('Kd_Unit' => $Kd_Unit);
        $data = array(
            $this->MRef_Unit->Kd_Pesanan => $dataPesanan
        );
        $this->MRef_Unit->updateBy($data,$field);

        if ($this->db->trans_status() === FALSE) {
            echo json_encode(array("status" => false));
            $this->db->trans_rollback();
            return FALSE;
        } 
        else {
            echo json_encode(array("status" => true));
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function tambahFilterPost(){
        $Tgl_Masuk  = $this->input->post("Tanggal_Masuk");
        $Tgl_Keluar = $this->input->post("Tanggal_Keluar");

        echo json_encode(array("status" => true, "Tgl_Masuk" => $Tgl_Masuk, "Tgl_Keluar" => $Tgl_Keluar));
    }

    function refUnitPesananDelete(){
        exit('delete by Kd_Pesanan');
    }

    function pesananKasir($id){
       if(!isLogin()){
            redirect('login');
        }
        
        $pageTitle = "Pelunasan Unit";
        $thisData = $this->MRef_Unit->getDataByQuery(array('Kd_Unit'),array($id),'row');
        // var_dump($thisData);exit;
        if($thisData->Kd_Pesanan != null && $thisData->Kd_Pesanan == ''){
            redirect(base_url("refUnitPesanan"));
        }

        $paramater['dataUnit']      = $this->MRef_Unit->getAll();
        $paramater['dataMerk']      = $this->MRef_Merk->getAll();
        $paramater['dataVarian']    = $this->MRef_Varian->getAll();
        $paramater['dataType']      = $this->MRef_Type->getAll();
        $paramater['pageTitle']     = $pageTitle;
        $paramater['thisData']      = $thisData;
        $paramater['inputStatus']   = 1;

        $this->load->view('backEnd/refPesanan/formRefPesanan',$paramater);
    }

    function exportExcel($Tgl_Masuk=null,$Tgl_Keluar=null){

        $pageTitle = "Data Stok Unit";
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);//activate worksheet number 1
        $this->excel->getActiveSheet()->setTitle('worksheet');//name the worksheet
        $formatHarga = '#,##0';
        $styleBorder = array(
          'borders' => array(
              'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );

        //set cell A1 content with some text
        $this->excel->getActiveSheet()
            ->setCellValue('A3', 'DATA STOK UNIT')
            ->setCellValue('A5', 'NO')
            ->setCellValue('B5', 'MERK')
            ->setCellValue('C5', 'VARIAN')
            ->setCellValue('D5', 'TIPE')
            ->setCellValue('E5', 'TAHUN')
            ->setCellValue('F5', 'NO PLAT')
            ->setCellValue('G5', 'TANGGAL MASUK')
            ->setCellValue('H5', 'HARGA BELI')
            ->setCellValue('I5', 'BIAYA PERAWATAN')
            ->setCellValue('J5', 'BIAYA BROKER')
            ->setCellValue('K5', 'TOTAL MODAL')
            ->setCellValue('L5', 'HARGA PASAR')
        ;

        $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setSize(14);//change the font size
        $cell = array('A3','A5','B5','C5','D5','E5','F5','G5','H5','I5','J5','K5','L5');
        foreach ($cell as $key => $value) {
            $column = substr($value,0,1);
            $this->excel->getActiveSheet()->getStyle($value)->getFont()->setBold(true);//make the font become bold
            $this->excel->getActiveSheet()->getStyle($value)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->excel->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
        }
        
        if(!$Tgl_Masuk && !$Tgl_Keluar){
            $dataUnit = $this->MRef_Unit->getQueryInner();
        }else{
            $dataFilter = $this->MRef_Unit->getQueryUnit('left')->where(["tt.Kd_Transaksi is null" => NULL, "Tgl_Masuk >=" => $Tgl_Masuk." 00:00:00", "Tgl_Masuk <" => $Tgl_Keluar." 23:59:59"])->get()->result();
            $dataUnit = $dataFilter;
        }

        $countDataUnit = count($dataUnit);
        if($dataUnit){
            foreach ($dataUnit as $key => $value) {
                // for ($char = 'A'; $char <= 'N'; $char++) { //LOOPING VARCHAR A TO N
                $keyd=$key+6;
                $this->excel->getActiveSheet()->setCellValue('A'.$keyd,$key+1)
                    ->setCellValue('B'.$keyd,$value->Nm_Merk)
                    ->setCellValue('C'.$keyd,$value->Nm_Varian)
                    ->setCellValue('D'.$keyd,$value->Nm_Type)
                    ->setCellValue('E'.$keyd,$value->Tahun)
                    ->setCellValue('F'.$keyd,$value->No_Plat)
                    ->setCellValue('G'.$keyd,$value->Tgl_Masuk)
                    ->setCellValue('H'.$keyd,$value->Harga_Beli)
                    ->setCellValue('I'.$keyd,$value->Biaya_Perawatan)
                    ->setCellValue('J'.$keyd,$value->Biaya_Broker)
                    ->setCellValue('K'.$keyd,$value->Total_Modal)
                    ->setCellValue('L'.$keyd,$value->Harga_Pasar);


                $this->excel->getActiveSheet()->getStyle('A'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('B'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('C'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('D'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('E'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('F'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('G'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('H'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('I'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('J'.$keyd)->applyFromArray($styleBorder);
                $this->excel->getActiveSheet()->getStyle('K'.$keyd)->applyFromArray($styleBorder)->getNumberFormat()->setFormatCode($formatHarga);
                $this->excel->getActiveSheet()->getStyle('L'.$keyd)->applyFromArray($styleBorder);

            }
        }else{
            $this->excel->getActiveSheet()->setCellValue('A6','Data Kosong');
        }

        $this->excel->getActiveSheet()->mergeCells('A3:L3'); //merge cell
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); //set aligment to center         
        $this->excel->getActiveSheet()->getStyle('A5:L5')->applyFromArray($styleBorder);

        $filename=$pageTitle.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}