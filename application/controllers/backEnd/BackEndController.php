<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BackEndController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'Map_user',
                'Map_user_akses',
                'MRef_Unit',
                'MRef_Bpkb'
            ));
    }

    function updateApps(){
        if(!isLogin()){
            redirect('login');
        }

        $path = FCPATH;
        $chdir = chdir($path);
        if($chdir){
            $command = "git status";
            $output = exec($command, $output_command);
            var_dump($output_command);
            echo "<pre>$output</pre>";
        }else{
            echo "else";
        }

    }

    public function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $parameter['refUnit'] = $this->MRef_Unit->getQueryUnit("left")->where('tt.Kd_Transaksi is null', NULL)->get()->num_rows();
        $parameter['refUnitKeluar'] = $this->MRef_Unit->getQueryUnit("right")->get()->num_rows();
        $parameter['refBpkb'] = $this->MRef_Bpkb->getQuery()->get()->num_rows();

        // var_dump($this->session->userdata('userId'));
        // var_dump($this->session->all_userdata());exit;
        $this->load->view('dashboard',$parameter);
    }

    public function login()
    {   
        if(isLogin()){
            redirect(base_url());
        }

        $pageTitle = "Login";
        $namelevel = $this->Map_user_akses->getAll();
        if(!empty($_POST)){

            // start, input->post adalah data dari user yang diketik (dimasukkan)
            $user               = $this->input->post('username');
            $pass               = $this->input->post('password');
            $Kd_Level_Akses        = $this->input->post('Kd_Level_Akses');
            // end.

            //cek login dari database
            $data = $this->Map_user->cekLogin($user,$pass,$Kd_Level_Akses);
            
            //sebuah kondisi apabila data yang dimasukkan salah oleh user maka akan muncul 'invalid username and password, silahkan ulangi kembali', jika benar maka simpan data session user
            if (!$data){
                $CI =& get_instance();
                $pesan = "Username atau Password yang dimasukkan Salah.";
                $CI->session->set_flashdata('error',$pesan);
            } else {
                    $newdata = array(
                        'username'          => $data['username'],
                        'level_akses'       => $data['Kd_Level_Akses'],
                        'waktu'             =>(string)convertDateTime(new dateTime(),'Y-m-d H:i:s'),
                        'userId'            => TRUE
                    );
                $this->session->set_userdata($newdata);
            }

            // mengecek apabila data session user benar maka menuju halaman yang ditentukan, apabila salah maka muncul 'invalid username and password, silahkan ulangi kembali'.
            if($this->session->userdata("userId") == TRUE){
                redirect(base_url());
            }else{
                $CI =& get_instance();
                $pesan = "Username & Password yang anda masukkan Salah";
                $CI->session->flashdata('error',$pesan);
            }
        }

        // data yang akan dilempar ke view
        $data['namelevel']                        = $namelevel;
        $data['pageTitle']                        = $pageTitle;
        $this->load->view('index', $data);
    }

    public function logout()
    {
        $CI =& get_instance();
        $data = array(
            'username',
            'password',
            'Kd_Level_Akses',
            'userId'
            );

        // jika user mengklik tombol logout maka session data user akan di hapus dan kembali ke halaman login
        $this->session->unset_userdata($data);
        $CI->session->sess_destroy();
        redirect('login');
    }   

    public function laporan($id)
    {

        $peserta            = $this->Mcrud->getAll();
        $nilai              = $this->Mnilai->getAll();
        $laporan            = 

        // var_dump($peserta->nama);die;

        $data['values']         = $this->Mcrud->getAll();
        

        // set nama file laporan
        $pdfFilePath = strtoupper("laporan").".pdf";

        $data['dataNilai']      = $nilai;
        $data['dataPeserta']    = $peserta;
        $data['pdf']            = $pdfFilePath;

        // mengambil fungsi dari mPDF itu sendiri
        $this->load->library('m_pdf');

        // mengeset ukuran mPDF
        $mpdf = new mPDF('utf-8','F4');
        $html = $this->load->view('laporanPdf',$data,true);


        // membuat halaman pdf dengan kententuan variabel $html
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, "I");

    }

    public function add(){
        $pageTitle = "Tambah";

        if(!empty($_POST)){

            $nama               = $this->input->post('nama');
            $tempat_lahir       = $this->input->post('tempat_lahir');
            $tanggal_lahir      = $this->input->post('tanggal_lahir');
            $no_telp            = $this->input->post('no_telp');
            $email              = $this->input->post('email');
            $alamat             = $this->input->post('alamat');

            // fungsi dari framework untuk security
            $this->db->trans_begin();

            $data = array(
                $this->Mcrud->nama              => $nama,
                $this->Mcrud->tempat_lahir      => $tempat_lahir,
                $this->Mcrud->tanggal_lahir     => $tanggal_lahir,
                $this->Mcrud->no_telp           => $no_telp,
                $this->Mcrud->email             => $email,
                $this->Mcrud->alamat            => $alamat
            );


            $this->Mcrud->insert($data);

            transStatus('data ',0,null,'add');

            transStatus('data ',1,null,'crud');
            
        }


        $data['pageTitle'] = $pageTitle;
        $this->load->view('formProses',$data);
    }

    public function update($id)
    {
        $pageTitle = "Update";

        $thisData = $this->Mcrud->getDataBy(array('id'),array($id),'row');
        if(!empty($_POST)) {

            $kodeId             = $thisData->id;
            $nama               = $this->input->post('nama');
            $tempat_lahir       = $this->input->post('tempat_lahir');
            $tanggal_lahir      = $this->input->post('tanggal_lahir');
            $no_telp            = $this->input->post('no_telp');
            $email              = $this->input->post('email');
            $alamat             = $this->input->post('alamat');

            $field = $kodeId;

            $this->db->trans_begin();

            $data = array(
                $this->Mcrud->nama              => $nama,
                $this->Mcrud->tempat_lahir      => $tempat_lahir,
                $this->Mcrud->tanggal_lahir     => $tanggal_lahir,
                $this->Mcrud->no_telp           => $no_telp,
                $this->Mcrud->email             => $email,
                $this->Mcrud->alamat            => $alamat
            );

            $this->Mcrud->update($data,$field);


            transStatus('data ',0,null,'update');

            transStatus('data ',1,null,'crud');
            
        }


        $data['pageTitle']  = $pageTitle;
        $data['thisData']   = $thisData;

        $this->load->view('formProses',$data);
    }


    function delete($id){

        // menghapus data dengan id
        $delete = $this->Mcrud->deleteDataBy(array('id' => $id ));

        // mengecek apabila data dengan id sudah benar maka terhapus dan menuju ke halaman yang di tentukan
        if($delete == true){
            redirect(base_url('crud'));
        }
    }
    
    function simulasi(){
        // var_dump('test');exit;
        $pageTitle = "Simulasi";

        $parameter['pageTitle']  = $pageTitle;
        $this->load->view("vSimulasi",$parameter);
    }

}
