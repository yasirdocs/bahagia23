<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefTypeController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk',
                'MRef_Varian',
                'MRef_Type'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar Type";
        $paramater['rows']      = $this->MRef_Type->getDataByQuery();

        $this->load->view('backEnd/refType/viewRefType',$paramater);
    }

    function add(){
        if(!isLogin()){
            redirect('login');
        }
        $pageTitle = "Tambah Type";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->MRef_Type->Kd_Merk              => $this->input->post('Kd_Merk'),
                $this->MRef_Type->Kd_Varian              => $this->input->post('Kd_Varian'),
                $this->MRef_Type->Nm_Type              => $this->input->post('Nm_Type')
            );

            $this->MRef_Type->insert($data);

            transStatus('Data ',0,null,'refTypeAdd');
            transStatus('Data ',1,null,'refType');
        }


        $paramater['pageTitle'] = $pageTitle;
        $paramater['dataMerk']      = $this->MRef_Merk->getAll();
        $paramater['dataVarian']    = $this->MRef_Varian->getAll();
        $this->load->view('backEnd/refType/formRefType',$paramater);
    }

    function update($id){
        if(!isLogin()){
            redirect('login');
        }
        $pageTitle = "Ubah Type";

        $thisData = $this->MRef_Type->getDataBy(array('Kd_Type'),array($id),'row');
        if(!empty($_POST)) {
            $kodeId         = $thisData->Kd_Type;
            $field          = array('Kd_Type' => $kodeId);

            $this->db->trans_begin();

            $data = array(
                $this->MRef_Type->Kd_Merk              => $this->input->post('Kd_Merk'),
                $this->MRef_Type->Kd_Varian              => $this->input->post('Kd_Varian'),
                $this->MRef_Type->Nm_Type              => $this->input->post('Nm_Type')
            );

            $this->MRef_Type->updateBy($data,$field);
            transStatus('Data ',0,null,'refTypeUpdate/'.$field);
            transStatus('Data ',1,null,'refType');
            
        }


        $paramater['pageTitle']  = $pageTitle;
        $paramater['thisData']   = $thisData;
        $paramater['dataMerk']      = $this->MRef_Merk->getAll();
        $paramater['dataVarian']    = $this->MRef_Varian->getAll();

        $this->load->view('backEnd/refType/formRefType',$paramater);
    }

    function delete($id){
        if(!isLogin()){
            redirect('login');
        }
        $this->MRef_Type->deleteDataBy(array('Kd_Type' => $id ));
        echo json_encode(array("status" => TRUE, "pesan" => "Data", "redirect" => "refType"));
    }

}