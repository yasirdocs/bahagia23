<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefBpkbController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk',
                'MRef_Unit',
                'MRef_Bpkb',
                'MRef_Status_Bpkb'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar Bpkb";
        $paramater['rows']      = $this->MRef_Bpkb->getDataByQuery();

        $this->load->view('backEnd/refBpkb/viewRefBpkb',$paramater);
    }

    function add(){
        if(!isLogin()){
            redirect('login');
        }
        $pageTitle = "Tambah Bpkb";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->MRef_Bpkb->Kd_Status_Bpkb       => $this->input->post('Kd_Status_Bpkb'),
                $this->MRef_Bpkb->Kd_Unit              => $this->input->post('Kd_Unit'),
                $this->MRef_Bpkb->Nm_Bpkb              => $this->input->post('Nm_Bpkb'),
                $this->MRef_Bpkb->No_Bpkb              => $this->input->post('No_Bpkb'),
                $this->MRef_Bpkb->No_Rangka            => $this->input->post('No_Rangka'),
                $this->MRef_Bpkb->No_Mesin             => $this->input->post('No_Mesin'),
                $this->MRef_Bpkb->Alamat_Bpkb          => $this->input->post('Alamat_Bpkb'),
                $this->MRef_Bpkb->Keterangan_Bpkb      => $this->input->post('Keterangan_Bpkb'),
                $this->MRef_Bpkb->Tgl_Masuk_Bpkb       => date('Y-m-d')
            );

            $this->MRef_Bpkb->insert($data);

            transStatus('Data ',0,null,'refBpkbAdd');
            transStatus('Data ',1,null,'refBpkb');
        }


        $paramater['pageTitle']           = $pageTitle;
        $paramater['dataMerk']            = $this->MRef_Merk->getAll();
        $paramater['dataUnit']            = $this->MRef_Unit->getAll();
        $paramater['dataStatusBpkb']      = $this->MRef_Status_Bpkb->getAll();
        $this->load->view('backEnd/refBpkb/formRefBpkb',$paramater);
    }

    function update($id){
        if(!isLogin()){
            redirect('login');
        }
        $pageTitle = "Ubah Bpkb";

        $thisData = $this->MRef_Bpkb->getDataBy(array('Kd_Bpkb'),array($id),'row');
        if(!empty($_POST)) {
            $kodeId         = $thisData->Kd_Bpkb;
            $field          = array('Kd_Bpkb' => $kodeId);

            $this->db->trans_begin();

            $data = array(
                $this->MRef_Bpkb->Kd_Status_Bpkb       => $this->input->post('Kd_Status_Bpkb'),
                $this->MRef_Bpkb->Nm_Bpkb              => $this->input->post('Nm_Bpkb'),
                $this->MRef_Bpkb->No_Bpkb              => $this->input->post('No_Bpkb'),
                $this->MRef_Bpkb->No_Rangka            => $this->input->post('No_Rangka'),
                $this->MRef_Bpkb->No_Mesin             => $this->input->post('No_Mesin'),
                $this->MRef_Bpkb->No_Plat              => $this->input->post('No_Plat'),
                $this->MRef_Bpkb->Alamat_Bpkb          => $this->input->post('Alamat_Bpkb'),
                $this->MRef_Bpkb->Keterangan_Bpkb      => $this->input->post('Keterangan_Bpkb'),
                $this->MRef_Bpkb->Tgl_Masuk_Bpkb       => date('Y-m-d')
            );

            $this->MRef_Bpkb->updateBy($data,$field);
            transStatus('Data ',0,null,'refBpkbUpdate/'.$field);
            transStatus('Data ',1,null,'refBpkb');    
        }

        $paramater['pageTitle']  = $pageTitle;
        $paramater['thisData']   = $thisData;
        $paramater['dataUnit']      = $this->MRef_Unit->getAll();
        $paramater['dataStatusBpkb']      = $this->MRef_Status_Bpkb->getAll();
        
        $this->load->view('backEnd/refBpkb/formRefBpkb',$paramater);
    }

    function delete($id){
        if(!isLogin()){
            redirect('login');
        }
        $this->MRef_Bpkb->deleteDataBy(array('Kd_Bpkb' => $id ));
        echo json_encode(array("status" => TRUE, "pesan" => "Data", "redirect" => "refBpkb"));
    }

    function serahBpkb($id){
        if(!isLogin()){
            redirect('login');
        }
        if($id == null || $id == "undefined"){
            redirect('login');
        }
        $thisData = $this->MRef_Bpkb->getDataByQuery(array('Kd_Bpkb'),array($id),'row');
        
        $pdfFilePath                   = strtoupper("Cetak BPKB").".pdf";
        $paramater['pageTitle']        = $pdfFilePath;
        $paramater['thisData']         = $thisData;


        $this->load->library('M_pdf');
        $mpdf = new \Mpdf\Mpdf([
            'format' => 'A4',
        ]);

        $html = $this->load->view('backEnd/pdf/viewPenyerahanBpkb',$paramater,true);
        $mpdf->WriteHTML($html);
        $mpdf->Output($pdfFilePath, "I");
    }

    function tambahPengambilPost(){
        if(!empty($_POST)){
            $id             = $this->input->post("Kd_Bpkb");
            $thisData       = $this->MRef_Bpkb->getDataBy(array('Kd_Bpkb'),array($id),'row');
            $kodeId         = $thisData->Kd_Bpkb;
            $field          = array('Kd_Bpkb' => $kodeId);

            $this->db->trans_begin();
            $data = array(
                $this->MRef_Bpkb->Nm_Pengambil                  => $this->input->post('Nm_Pengambil'),
                $this->MRef_Bpkb->Alamat_Pengambil              => $this->input->post('Alamat_Pengambil'),
                $this->MRef_Bpkb->No_Ktp_Pengambil              => $this->input->post('No_Ktp_Pengambil'),
                $this->MRef_Bpkb->Tgl_Penyerahan_Bpkb           => date('Y-m-d')
            );

            $this->MRef_Bpkb->updateBy($data,$field);

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                redirect('/dashboard');
            }else{
                $this->db->trans_commit();
                
                $id = $this->input->post("Kd_Bpkb");
                $thisData = $this->MRef_Bpkb->getDataBy(array('Kd_Bpkb'),array($id),'row');
                $pdfFilePath                   = strtoupper("Cetak BPKB").".pdf";
                $paramater['pageTitle']        = $pdfFilePath;
                $paramater['thisData']         = $thisData;
                $this->load->library('M_pdf');
                $mpdf = new \Mpdf\Mpdf([
                    'format' => 'A4',
                ]);

                $html = $this->load->view('backEnd/pdf/viewPenyerahanBpkb',$paramater,true);
                $mpdf->WriteHTML($html);
                $mpdf->Output($pdfFilePath, "I");        
            }
        }else{
            exit("Maaf, Anda tidak punya akses disini");
        }
    }

    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('Nm_Pengambil') == ''){
            $data['inputerror'][] = 'Nm_Pengambil';
            $data['error_string'][] = 'Nama Pengambil Wajib diisi';
            $data['status'] = FALSE;
        }

        if($this->input->post('No_Ktp') == ''){
            $data['inputerror'][] = 'No_Ktp';
            $data['error_string'][] = 'Nomor Ktp Wajib diisi';
            $data['status'] = FALSE;
        }

        if($this->input->post('Alamat_Pengambil') == ''){
            $data['inputerror'][] = 'Alamat_Pengambil';
            $data['error_string'][] = 'Alamat Wajib diisi';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }


}