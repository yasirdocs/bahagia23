<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefMerkController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar Merk";
        $paramater['rows']      = $this->MRef_Merk->getAll();

        $this->load->view('backEnd/refMerk/viewRefMerk',$paramater);
    }

    function add(){
        if(!isLogin()){
            redirect('login');
        }

        $pageTitle = "Tambah Merk";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->MRef_Merk->Nm_Merk              => $this->input->post('Nm_Merk')
            );

            $this->MRef_Merk->insert($data);

            transStatus('Data ',0,null,'refMerkAdd');
            transStatus('Data ',1,null,'refMerk');
        }


        $paramater['pageTitle'] = $pageTitle;
        $this->load->view('backEnd/refMerk/formRefMerk',$paramater);
    }

    function update($id)
    {
        if(!isLogin()){
            redirect('login');
        }

        $pageTitle = "Ubah Merk";

        $thisData = $this->MRef_Merk->getDataBy(array('Kd_Merk'),array($id),'row');
        if(!empty($_POST)) {
            $kodeId         = $thisData->Kd_Merk;
            $field          = array('Kd_Merk' => $kodeId);

            $this->db->trans_begin();

            $data = array(
                $this->MRef_Merk->Nm_Merk              => $this->input->post('Nm_Merk')
            );

            $this->MRef_Merk->updateBy($data,$field);
            transStatus('Data ',0,null,'refMerkUpdate/'.$field);
            transStatus('Data ',1,null,'refMerk');
            
        }


        $paramater['pageTitle']  = $pageTitle;
        $paramater['thisData']   = $thisData;

        $this->load->view('backEnd/refMerk/formRefMerk',$paramater);
    }

    function delete($id){
        if(!isLogin()){
            redirect('login');
        }
        $this->MRef_Merk->deleteDataBy(array('Kd_Merk' => $id));
        echo json_encode(array("status" => TRUE, "pesan" => "Data", "redirect" => "refMerk"));
    }


}