<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RefVarianController extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'MRef_Merk',
                'MRef_Varian'
            ));
    }

    function index()
    {   
        if(!isLogin()){
            redirect('login');
        }

        $paramater['pageTitle'] = "Daftar Varian";
        $paramater['rows']      = $this->MRef_Varian->getDataByQuery();

        $this->load->view('backEnd/refVarian/viewRefVarian',$paramater);
    }

    function add(){
        if(!isLogin()){
            redirect('login');
        }

        $pageTitle = "Tambah Varian";

        if(!empty($_POST)){
            $this->db->trans_begin();

            $data = array(
                $this->MRef_Varian->Kd_Merk                 => $this->input->post('Kd_Merk'),
                $this->MRef_Varian->Nm_Varian               => $this->input->post('Nm_Varian')
            );

            $this->MRef_Varian->insert($data);

            transStatus('Data ',0,null,'refVarianAdd');
            transStatus('Data ',1,null,'refVarian');
        }


        $paramater['pageTitle'] = $pageTitle;
        $paramater['dataMerk']  = $this->MRef_Merk->getAll();
        $this->load->view('backEnd/refVarian/formRefVarian',$paramater);
    }

    function update($id)
    {
        if(!isLogin()){
            redirect('login');
        }

        $pageTitle = "Ubah Varian";

        $thisData = $this->MRef_Varian->getDataBy(array('Kd_Varian'),array($id),'row');
        if(!empty($_POST)) {
            $kodeId         = $thisData->Kd_Varian;
            $field          = array('Kd_Varian' => $kodeId);

            $this->db->trans_begin();

            $data = array(
                $this->MRef_Varian->Kd_Merk                => $this->input->post('Kd_Merk'),
                $this->MRef_Varian->Nm_Varian              => $this->input->post('Nm_Varian')
            );

            $this->MRef_Varian->updateBy($data,$field);
            transStatus('Data ',0,null,'refVarianUpdate/'.$field);
            transStatus('Data ',1,null,'refVarian');
            
        }


        $paramater['dataMerk']  = $this->MRef_Merk->getAll();
        $paramater['pageTitle']  = $pageTitle;
        $paramater['thisData']   = $thisData;

        $this->load->view('backEnd/refVarian/formRefVarian',$paramater);
    }


    function delete($id){
        if(!isLogin()){
            redirect('login');
        }
        $this->MRef_Varian->deleteDataBy(array('Kd_Varian' => $id ));
        echo json_encode(array("status" => TRUE, "pesan" => "Data", "redirect" => "refVarian"));
    }

}