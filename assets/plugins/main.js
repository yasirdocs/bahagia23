
$(document).ready(function(){
    $(document).on('click','.modalView',function(){
        var url = $(this).attr('data-url');
        $('#viewModal').modal('show');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "JSON",
            success: function(data) {
                if(data.status == true){
                    $('#tableModal').html(data.html);
                }else{
                    swal('Oops...','Data Tidak ditemukan.','error');
                }
            }
        }); //ajax
    }); //document click

    $(document).on('click','.alertHapus',function(){
        var url = $(this).attr('data-url');
        swal({title: 'Apakah Kamu yakin?',text: "Data yang sudah dihapus, tidak dapat dikembalikan",type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'Ya, Hapus'
        }).then(function (isConfirm) {
            if(isConfirm){
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    success: function(data) {
                        if(data.status == true){
                                swal({ title: "Berhasil!", text: data.pesan+" Berhasil dihapus", type: "success", confirmButtonText: "Ok"}).then(function(isConfirm){
                                        if(isConfirm){
                                            window.location = data.redirect;
                                        }
                                });
                        } //if
                    }
                }); //ajax
            };
        }).catch(swal.noop); //then
    }); //document click

});